#include <stdio.h>
#include <stdlib.h>

__attribute__((constructor)) static void init() {
  setvbuf(stdin, 0, 2, 0);
  setvbuf(stdout, 0, 2, 0);
}

static void secret() {
  printf("Nice work!\n");
  system("/bin/sh");
}

int main() {
  char name[16];
  printf("What's your name?\n");
  scanf("%s", name);
  printf("Hello, %s!\n", name);
}
