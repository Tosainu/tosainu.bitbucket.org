layout: true
class: center, middle, inverse
---
# はじめまして, Linux.

2014/08/23  
kosenconf-085nagoya2  

とさいぬ - @myon\_\_\_
---
# みょん (はじめまして)
---
layout: false
## とさいぬ - @myon\_\_\_

.left-column[
![img](icon.jpg)
]

.right-column[
### 誰だよお前
* <del>ToyotaNCT 4年目</del> 16歳JK
* <del>一応電気科らしい</del> 普通科らしい
* 主にプログラミングやネットワーク関係について独学で勉強している
* NCTロボコン経験有(2012PIT)
* 実はkosenconf-079mie3に出席していました


* Twitter [@myon\_\_\_](https://twitter.com/myon___)
* Website [myon.info](http://myon.info)
]
---
## とさいぬ - @myon\_\_\_

.left-column[
![img](icon.jpg)
]

.right-column[
### 性格？
* "みょん" ばっか言ってる
* Micro$oftは嫌いらしい

### 好き
* (Arch)Linux
* Vim
* 写真
* Xperia 2011
]
---
## Works

### Raspberry Pi搭載ラジコン

* Raspberry Piを搭載したラジコンカー
* Node.jsとSocket.ioを使いWebブラウザからリアルタイムな操縦を実現
* カメラからのストリーミング機能も搭載
* 昨年のこうよう祭(文化祭)で展示/実演
* 今年も改良して展示する(かも)

ブログ記事: <http://tosainu.wktk.so/view/324>

---
外観

![img](rc.jpg)
---
操作画面

![img](cpl.png)
---
## Works

### [twitpp](https://github.com/Tosainu/twitpp)
* C++11で書かれた俺得Twitterライブラリ (開発中)
* Boost.Asioというライブラリを使っている
* REST APIはもちろん, Streaming APIにも対応
* RaspberryPiでも動く (ビルド時間30分)
* これを使ってTwitterクライアントを開発中という噂

```cpp
#include <iostream>

auto main() -> int {
  auto myon = []() { std::cout << "lambda!!!!" << std::endl; };
  myon();
}
```
---
```cpp
twitpp::OAuth::Account account("CONSUMER", "CONSUMER_SECRET");

// get oauth token
std::cout << account.get_authorize_url() << std::endl;
account.get_oauth_token(pin);

twitpp::OAuth::Client oauth(io_service, ctx, account);

// update
std::map<std::string, std::string> pya;
pya["status"] = "test";
oauth.post("api.twitter.com", "/1.1/statuses/update.json", pya,
           [](int& status, std::string& text) {
  std::cout << text << std::endl;
  text.assign("");
});

// userstream
oauth.get("userstream.twitter.com", "/1.1/user.json", [](int& status, std::string& text) {
  std::cout << text << std::endl;
  text.assign("");
});
```
---
layout: true
class: center, middle, inverse
---
# よろしくです ∩(＞◡＜\*)∩
---
layout: false
## 話すこと

* What is Linux ?
* Linuxをメインで使うまでの経緯
* その後
* どんな感じに使ってるかの実演
* まとめ
---
layout: true
class: center, middle, inverse
---
# What is Linux ?
---
layout: false
## What is Linux ?

### そもそも何よ？

* 1991年, Linus Torvalds氏によって開発が始まったUnixライクなOS
* GNU GPL v2ライセンスの元で開発されている自由ソフトウェア
* x86をはじめARM, Power, SPARC等多くのプラットフォームに移植されている
* サーバやデスクトップとしてのほか, 携帯電話やテレビなどの組み込みシステムでも使われている

注: Linuxはカーネルを指す言葉だが, そのシステム全体(Linuxディストリビューション)を指す言葉としても使われる

---
## What is Linux ?

### システムの特徴

* マルチユーザ, マルチタスク
* 多くのファイルシステムをサポート<br>Ext, Reiser, XFS, Btrfsなど
* /を最上位としたツリー構造のディレクトリ (ドライブの概念はない)
* テキストファイルによる細かい設定管理 (レジストリｱｱｱｯ...)
* 優れたコマンドやユーティリティ, Shell環境を提供<br>(GUIなしでもすべての操作可能)
* パッケージ管理システムによる依存関係の解決

---
#### Windowsの場合

![img](winfs.png)

(マイ)コンピュータから接続したディスク等にアクセス
---
#### Linuxの場合

![img](linuxfs.png)

/(Root)を最上位としたツリー状のディレクトリ. 任意の場所にディスクをマウント.
---
layout: true
class: center, middle, inverse
---
# Linuxをメインで使うまでの経緯
---
layout: false
## Linuxをメインで使うまでの経緯

### 〜2011
* 完全なWindows信者だった
* 中学時代はVisual Basic .NETでゴミ生産してた
* 1年のコンピュータリテラシでWindows7を模した自己紹介パワポ作成

---
## Linuxをメインで使うまでの経緯

### 〜2011
* 完全なWindows信者だった
* 中学時代はVisual Basic .NETでゴミ生産してた
* 1年のコンピュータリテラシでWindows7を模した自己紹介パワポ作成

### 駄菓子菓子
* PCは与えてもらえたものの, 父親の使い古しまたは格安中古
* 当時: 1.5GHz-CPU, 1GB-RAM, VIA製GPUの**Windows Vista**ノート

---
## Linuxをメインで使うまでの経緯

### 〜2011
* 完全なWindows信者だった
* 中学時代はVisual Basic .NETでゴミ生産してた
* 1年のコンピュータリテラシでWindows7を模した自己紹介パワポ作成

### 駄菓子菓子
* PCは与えてもらえたものの, 父親の使い古しまたは格安中古
* 当時: 1.5GHz-CPU, 1GB-RAM, VIA製GPUの**Windows Vista**ノート

当時ロボコンにも参加していたのもあり開発環境だけでも改善したかった
---
## Linuxをメインで使うまでの経緯

### Ubuntu 10.04との出会い

* [KNOPPIX](http://www.knopper.net/knoppix/)を父親氏が使っていたのを思い出し同様のOSを探してみた
* Wubiとやらで簡単にデュアルブート環境が作れるっぽい

---
## Linuxをメインで使うまでの経緯

### Ubuntu 10.04との出会い

* [KNOPPIX](http://www.knopper.net/knoppix/)を父親氏が使っていたのを思い出し同様のOSを探してみた
* Wubiとやらで簡単にデュアルブート環境が作れるっぽい

### インストールした

* 早い! なにこれ! 20[sec]で起動した! Winの15分の1!!!!!!!!
* apt-getで何でも入る!!!インストーラ探しに行かなくて良い!!!!!
* ChromiumやGIMPなど, 一部使い慣れたソフトがnativeで使えた!!!!!!!
* Windowsより優れて高速で高機能なプログラミング環境!!!!!!

---
## Linuxをメインで使うまでの経緯

### Ubuntu 10.04との出会い

* [KNOPPIX](http://www.knopper.net/knoppix/)を父親氏が使っていたのを思い出し同様のOSを探してみた
* Wubiとやらで簡単にデュアルブート環境が作れるっぽい

### インストールした

* 早い! なにこれ! 20[sec]で起動した! Winの15分の1!!!!!!!!
* apt-getで何でも入る!!!インストーラ探しに行かなくて良い!!!!!
* ChromiumやGIMPなど, 一部使い慣れたソフトがnativeで使えた!!!!!!!
* Windowsより優れて高速で高機能なプログラミング環境!!!!!!

数ヶ月サブOSとして使った後, とうとうGomindows Vistaを上書き
---
layout: true
class: center, middle, inverse
---
# その後
---
layout: false
## その後

### 〜2013前半
* Ubuntu飽きてきて, Linuxディストリビューション試しまくってた
* Ubuntu派生, Fedora, CentOS, OpenSUSE, Gentoo等, 有名なのはほとんど試した
* Fedoraをメインにするものの, しっくり来るのがなかった

---
## その後

### 〜2013前半
* Ubuntu飽きてきて, Linuxディストリビューション試しまくってた
* Ubuntu派生, Fedora, CentOS, OpenSUSE, Gentoo等, 有名なのはほとんど試した
* Fedoraをメインにするものの, しっくり来るのがなかった

そんな時, あるディストリに出会う!

---
layout: true
class: center, middle, inverse
---
![img](archlinux-logo-light-1200dpi.png)
---
# どんな感じに使ってるかの実演
---
# まとめ
---
## みなさんLinuxは最高なので使いましょう!
---
# (✿╹◡╹)ﾉ End
