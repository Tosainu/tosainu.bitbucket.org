class: left, middle

# FPGA/GPU/CPUが集積された<br />ヘテロSoC環境におけるプログラミング

.small[
2020/01/11 - 第61回 プログラミング・シンポジウム  
佐藤 健太・佐藤 幸紀 (豊橋技科大)
]

---

class: left, middle

- 資料  
.larger[<https://l.myon.info/prosym-61>]
- 関連ブログ記事
.larger[<https://blog.myon.info/entry/tags/fpga/>]

---

## Agenda

1. はじめに
2. Ultra96 を用いた Julia 集合の描画システム
3. Vivado HLS による FPGA 開発
4. SystemVerilog による FPGA 開発
5. 各種比較
6. まとめ

---

class: left, section-title-page

.section-title[
.section-number[
1.
]
.section-text[
はじめに
]
]

---

name: fpga-as
background-image: url(img/fpga_as_5g.svg)
background-position: 50% 70%
background-size: 82.5% auto

## FPGA: Field Programmable Gate Array

- 論理回路の仕様を与えるとそのとおりに振る舞う半導体素子
- 何度も再構成できる

---

count: false
template: fpga-as
background-image: url(img/fpga_as_cpu.svg)

.footnote.tiny[https://riscv.org/risc-v-logo/]

---

## ハードウェアアクセラレータとしての FPGA

- 集積回路のプロトタイピングとしての用途が主だった FPGA
- 近年の**著しい性能向上**・**設計ツールの高性能化**などにより  
ハードウェアアクセラレータとしても注目されている

---

class: no-slide-number
background-image: url(img/alveo.webp)

.footnote.tiny[https://www.xilinx.com/content/xilinx/en/products/boards-and-kits/alveo.html]

---

class: no-slide-number

## FPGA を搭載した SoC の登場

- Zynq .small[(Xilinx)] や SoC FPGA .small[(Intel)] など **FPGA を搭載した SoC** が登場
    - FPGA と CPU の密結合で **高い性能** と **開発の柔軟性** の両立を狙う

.flex-center.margin--1em[
.column-40.left[
![](img/zynq.png)
]
.column-40.right[
![](img/agilex.png)
]
]

.footnote.tiny.right[https://www.xilinx.com/products/silicon-devices/soc.html<br />https://www.intel.com/content/www/us/en/products/programmable/soc.html]

---

count: false
class: no-slide-number

## FPGA を搭載した SoC の登場

- Zynq .small[(Xilinx)] や SoC FPGA .small[(Intel)] など **FPGA を搭載した SoC** が登場
    - FPGA と CPU の密結合で **高い性能** と **開発の柔軟性** の両立を狙う

.flex-center.margin--1em[
.column-40.left[
![](img/zynq-soc.webp)
]
.column-40.right[
![](img/AgilexBoard.webp)
]
]

.footnote.tiny.right[http://zedboard.org/product/ultrazed-EG<br />https://www.mantaro.com/products/development-boards/intel-agilex-som.htm]

---

class: left, section-title-page

.section-title[
.section-number[
2.
]
.section-text[
Ultra96 を用いた  
Julia 集合の描画システム
]
]

---

background-image: url(img/ultra96.png)
background-position: 90% 85%
background-size: auto 50%

## Ultra96

- Zynq UltraScale+ MPSoC の開発ボードの1つ


- **高性能!** .small[(ZU3EG A484, LPDDR4 2GB, MiniDP, USB 3.0, Wi-Fi & BT, etc.)]
- **小型!** .small[(85mm x 54mm)]
- **安価!** .small[($249)]

.footnote.tiny[https://www.96boards.org/product/ultra96/]

---

background-image: url(img/zynq-eg-block.png)
background-position: 50% 82.5%
background-size: auto 82%
class: no-slide-number

## Zynq UltraScale+ EG

.footnote.tiny[https://www.xilinx.com/products/silicon-devices/soc/zynq-ultrascale-mpsoc.html]

---

class: no-slide-number

.flex[
.column-55[
## 作ってみた

- [Tosainu/ultra96-fractal](https://github.com/Tosainu/ultra96-fractal)
]
.column-45[
<blockquote class="twitter-tweet" data-dnt="true"><p lang="ja" dir="ltr">一度 HDL 書く開発してみたいなーってことで冬・春に HLS で書いたのを SystemVerilog で書き直してた。<a href="https://t.co/h7HXFqsaHa">https://t.co/h7HXFqsaHa</a> と比較して動作クロック3倍 (100➡300MHz)、描画速度2.67倍 (6➡16fps) に改善できて、色も変えれるようになった。 <a href="https://t.co/GoizfFcpYa">pic.twitter.com/GoizfFcpYa</a></p>&mdash; +。:.ﾟ٩(๑ᐳ◡ᐸ๑)۶:.｡+ﾟ (@myon___) <a href="https://twitter.com/myon___/status/1163835624710795264?ref_src=twsrc%5Etfw">August 20, 2019</a></blockquote>
]
]

---

## 特徴

- **Julia 集合の高速描画**
    - 1920px ✕ 1080px @ **16.2 fps** を達成
- **ゲームパッドによるインタラクティブな操作**
- **描画パラメータのステータス表示**

--
class: no-slide-number
count: false
background-image: url(img/screen.webp)
background-position: 95% 95%
background-size: 45% auto

---

name: jse-arch
background-image: url(img/block.svg)
background-position: 50% 77.5%
background-size: 82.5% auto

## システムのアーキテクチャ

---


template: jse-arch
background-image: url(img/block_fpga.svg)

- **FPGA**: Julia 集合の描画専用回路を実装
    - 並列度の高い描画パイプラインで高速描画

---

template: jse-arch
background-image: url(img/block_kernel.svg)

- **APU** (Application Prosessing Unit): OS に Linux を採用
    - GPU やゲームパッドなどの利用が容易に

---

template: jse-arch
background-image: url(img/block_user.svg)

- **APU**: ユーザ空間に専用のアプリケーションを実装
    - 各デバイスに適切な指示を出し協調動作を実現

---

template: jse-arch

---

## 開発の流れ

1. FPGA に Julia 集合を描画するための専用回路の実装
2. FPGA に実装した回路と APU の接続関係の設計
3. 実装した回路のデバイスドライバの実装
4. 全体を制御し画面へ Julia 集合を表示するアプリケーションの実装
5. Linux のブートイメージを作成

---

## Julia 集合描画回路の実装

- Julia 集合の描画は FPGA に向いている
    - 小さな計算 \\(f(z) = z^2 + c\\) を何度も行うだけ
    - ピクセル間の依存がなく並列化が容易
- **Julia 集合を描画するだけの回路**を FPGA 上に実装する
    - C++ (HLS) または SystemVerilog で実装

---

## FPGA・APU の接続

- ソフトウェアから実装した回路に命令を与えたり、  
実装した回路が出力した画像を受け取れるようにしたい
- APU と接続したり IP (≒ ライブラリ) と組み合わせる
    - 少し前: Verilog などの HDL を利用
    - 今: Vivado の IP Integrator など  
    **GUI ベースで設計可能な開発環境**がある

---

background-image: url(img/bd.webp)
background-size: 95% auto

--
count: false
name: bd-1
background-position: 35% 80%
background-size: 275%

--
count: false
background-position: 52.5% 80%

--
count: false
background-position: 70% 80%

---
count: false
template: bd-1

--
count: false
background-position: -2.5% 80%

---

## デバイスドライバの実装

- Linux が使いたい
    - GPU や USB 接続のゲームパッドを使いたい
    - ソフトの開発で慣れたソフトウェアやライブラリを使いたい
- **映像の入力デバイス** として扱うためのデバイスドライバを実装

---

.flex[
.column-50.smaller[
```
root@fractal:~# media-ctl -p
Media controller API version 4.14.0

Media device information
------------------------
driver          xilinx-video
model           Xilinx Video Composite Device
serial          
bus info        
hw revision     0x0
driver version  4.14.0

Device topology
- entity 1: video_cap output 0 (1 pad, 1 link)
            type Node subtype V4L flags 0
            device node name /dev/video0
	pad0: Sink
		<- "a0000000.fractal":0 [ENABLED]

- entity 5: a0000000.fractal (1 pad, 1 link)
            type V4L2 subdev subtype Unknown flags 0
            device node name /dev/v4l-subdev0
	pad0: Source
		[fmt:RBG888_1X24/1920x1080 field:none]
		-> "video_cap output 0":0 [ENABLED]
```
]
.column-50[
- FPGA に実装した回路が `/dev/video0` や  
`/dev/v4l-subdev0` として  
認識されている!
]
]

---

## 制御・表示アプリケーションの実装

- 主な機能・役割
    - FPGA 側の回路から映像を受け取るための準備をする
    - ゲームパッドの入力をもとに描画パラメータを決め  
    FPGA 側の回路へ与える
    - 出力された画像を画面に表示する
- C++ で実装

---

exclude: true

コード紹介を入れる

---

## Linux ブートイメージの作成

- FPGA のコンフィグレーション、Linux システム、  
アプリケーションなどをまとめたブートイメージを作成する
- Xilinx の各種プラットフォームでは PetaLinux Tools が利用可能
    - ブートイメージを数コマンドで生成できる

---

class: left, section-title-page

.section-title[
.section-number[
3.
]
.section-text[
Vivado HLS による FPGA 開発
]
]

---

## Vivado HLS

- Xilinx 社が提供する**高位合成**の開発環境


- C、C++、SystemC を用いた設計が可能
- 豊富なライブラリが提供される
    - `<math.h>` で提供される数学関数
    - OpenCV 互換の関数
    - 任意精度のデータ型

---

exclude: true

簡単な C++ のコードが Verilog に変換される例をいくつかあげる？

---

class: no-slide-number

## HLS に向いた C++ のコード

- **単純な処理であること**
    - 原則として単純な論理回路ほど低遅延 (= **高い周波数で動作可**)
    - 結果として必要のない処理が発生しても良い
- **計算量が入力によって大きく変化しないこと**
    - 回路の並列度向上が容易

.footnote.small[\* 実装するアルゴリズムや対象の FPGA によってはこの限りではない]

---

name: loop-compare

## 例: Julia 集合を描画するコードの例

.flex[
.column-50[
```cpp
for (int y = 0; y < HEIGHT; y++) {
  for (int x = 0; x < WIDTH; x++) {
    auto z = /* ... */;
    int i = 0;
    while (i < 255 &&
           z.norm() < 2.0) {
      i += 1;
      z = z * z + c;
    }
    output.write(i);
  }
}
```
]
.column-50[
```cpp
for (int y = 0; y < HEIGHT; y++) {
  for (int x = 0; x < WIDTH; x++) {
    auto z = /* ... */;
    int i = 0;
    bool done = false;
    for (int w = 0; w < 255; w++) {
      done = done || z.norm() < 2.0;
      i = done ? i : i + 1;
      z = done ? z : z * z + c;
    }
    output.write(i);
  }
}
```
]
]

---

template: loop-compare

.overlay-right[
- `while` の終了条件が複雑
- 反復回数が `z` の初期値に依存

➔ **FPGA に優しくない**
]

---

template: loop-compare

.overlay-left[
- 反復回数は常に一定で  
コンパイル時に決定可能

➔ **FPGA に優しい!**
]

---

class: no-slide-number
name: rgb-func

## HLS でも効果的な C++ のコード

- Julia 集合の色つけに使う関数
\\[
\begin{aligned}
  r(t) &= 9   \times (1 - t)   t^3 \\\\
  g(t) &= 15  \times (1 - t)^2 t^2 \\\\
  b(t) &= 8.5 \times (1 - t)^3 t
\end{aligned}
\\]

---

background-image: url(img/color.svg)
background-size: auto 90%
class: no-slide-number

---
template: rgb-func

- 貴重な乗算用リソースを \\(f(z)\\) の計算以外で消費するのは避けたい
    - **「コンパイル時計算」**で定数化

---

## `constexpr` 関数

```cpp
// C++11 時代の constexpr 関数
constexpr std::uint64_t factorial(std::uint64_t n) {
  return n > 0 ? n * factorial(n - 1) : 1;
}

// C++14 + の constexpr 関数
constexpr auto factorial2(std::uint64_t n) {
  auto f = 1ull;
  for (auto i = 1ull; i <= n; ++i) f *= i;
  return f;
}

static_assert(factorial(10) == 3'628'800);
static_assert(factorial2(10) == 3'628'800);
```

---

## 色つけ関数の `constexpr` 化

```cpp
template <class T>
constexpr T constexpr_min(T a, T b) {
  return a < b ? a : b;
}

constexpr std::uint32_t f2i(double d) {
  return static_cast<std::uint32_t>(constexpr_min(d, 1.0) * 255.0);
}

constexpr std::uint32_t colorize(double t) {
  return (f2i(9.0 * (1.0 - t) * t * t * t) << 16) |                // red
         (f2i(8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t) * t) << 8) | // blue
         (f2i(15.0 * (1.0 - t) * (1.0 - t) * t * t));              // green
}
```

---

## コンパイル時配列構築

```cpp
constexpr std::uint32_t index2color(std::size_t i) {
    return colorize(static_cast<double>(i) / 255.0);
}

// 256 個の配列を構築し各要素を index2color(Index) で初期化する
constexpr std::array<std::uint32_t, 256> make_color_table() {
    return detail::make_array<256>(index2color);
}

// 各要素が
// { index2color(0), index2color(1), ..., index2color(255) } で
// 初期化された配列を作りたい!
static constexpr auto color_table = make_color_table();
```

---

## `std::make_index_sequence` .tiny[(since C++14)]

- コンパイル時に `N` から `0, 1, ..., N - 1` を作る helper

```cpp
#include <type_traits>
#include <utility>

static_assert(std::is_same_v<
                std::make_index_sequence<5>,
                std::index_sequence<0, 1, 2, 3, 4> >);
```

.footnote.tiny[https://en.cppreference.com/w/cpp/utility/integer_sequence]

---

## `std::make_index_sequence` の使い方

```cpp
template <class Func, std::size_t... Ns>
constexpr auto make_array_impl(Func f, std::index_sequence<Ns...>) {
  // Ns... を通して 0, 1, ..., N - 1 がコンパイル時に手に入る!
  return std::array{f(Ns)...};
}

template <std::size_t N, class Func>
constexpr auto make_array(Func f) {
  // make_array_impl に
  // std::index_sequence<N> = std::index_sequence<0, 1, ..., N - 1> な
  // オブジェクトも一緒に渡す
  return make_array_impl(f, std::make_index_sequence<N>{});
}
```

---

class: no-slide-number

## `std::make_index_sequence` 相当を実装する

```cpp
template <std::size_t... Ns>
struct index_sequence {};

template <std::size_t N, std::size_t... Ns>
struct make_index_sequence_impl
    : make_index_sequence_impl<N - 1, N - 1, Ns...> {};

template <std::size_t... Ns>
struct make_index_sequence_impl<0, Ns...> {
  using type = index_sequence<Ns...>;
};

template <std::size_t N>
using make_index_sequence = typename make_index_sequence_impl<N>::type;
```

.footnote.small[\* `template` の再帰深度制約の対応が必要かも]

---

## metashell で確認

```
> #msh mdb make_index_sequence<5>;           
For help, type "help".
Metaprogram started
(mdb) ft                              
make_index_sequence<5>;
` make_index_sequence
  + make_index_sequence_impl<5>
  | + make_index_sequence_impl<4, 4>
  | | + make_index_sequence_impl<3, 3, 4>
  | | | + make_index_sequence_impl<2, 2, 3, 4>
  | | | | + make_index_sequence_impl<1, 1, 2, 3, 4>
  | | | | | + make_index_sequence_impl<0, 0, 1, 2, 3, 4>
  | | | | | | ` make_index_sequence_impl<0, Ns...>
```

.footnote.tiny[https://github.com/metashell/metashell]

---

background-image: url(img/hls-result.png)
background-position: 50% -30%
background-size: 82.5% auto

## Vivado HLS のコンパイル結果レポート

--

.overlay-left[
大量のリソースや  
クロックを要求する処理が、  
ROM から値を読むだけの  
**数クロックで完結する処理に**
]

---

class: left, section-title-page

.section-title[
.section-number[
4.
]
.section-text[
SystemVerilog による FPGA 開発
]
]

---

## 実装内容

- HLS では自動生成されていた処理も記述する必要がある
    - Julia集合の描画パイプライン
    - 現在の処理内容を指すステートマシン
    - 次に処理する座標 \\\((x, y)\\) や \\(z_0\\) のカウンタ
    - Julia集合の色つけ回路
    - 制御信号 (座標 \\((0, 0)\\) の値が出力されるときHi，など) の生成
    - 描画パラメータなどを指定するレジスタ

---

background-image: url(img/sv_pipeline.svg)
background-position: 50% 70%
background-size: 65% auto

## 実装した回路のブロック図

---

## ステートマシン

- 一方向の状態遷移のみで完結したためリングカウンタを利用

```verilog
bit       [NUM_STAGES - 1:0] state0 = state0_end;
always_ff @(posedge clk) begin
  if (~resetn)
    state0 <= state0_init;
  else begin
    if (state0[NUM_STAGES - 1])
      state0 <= state0_begin;
    else
      state0 <= state0 << 1;
  end
end

```

---

## 描画パイプライン

- \\(f(z)\\) 計算のための記述はこれだけ

```verilog
for (genvar i = 0; i < NUM_PARALLELS; i++) begin
  always_ff @(posedge clk) begin
    zr2[i][0] <= zr[i] * zr[i];
    zi2[i][0] <= zi[i] * zi[i];
    zri[i][0] <= zr[i] * zi[i];
  end
end
```

---

## 描画パイプライン

- 値を `NUM_MULTIPLIER_STAGES` クロックの間伝搬するだけの記述

```verilog
for (genvar i = 0; i < NUM_PARALLELS; i++) begin
  for (genvar j = 1; j < NUM_MULTIPLIER_STAGES; j++) begin
    always_ff @(posedge clk) begin
      zr2[i][j] <= zr2[i][j - 1];
      zi2[i][j] <= zi2[i][j - 1];
      zri[i][j] <= zri[i][j - 1];
    end
  end
end
```

---

class: left, section-title-page

.section-title[
.section-number[
5.
]
.section-text[
各種比較
]
]

---

## コード行数

.flex-center[
| HLS 実装 | HDL 実装 |
| --: | --: |
| 163 | 730 |
]

- 空行などは除いた値
- HDL は実装対象が多くなるためコードも増加
    - 開発環境が提供するテンプレートが利用できる箇所あり

---

## コンパイル時間

.flex-center[
| | HLS 実装 | HDL 実装 |
| --- | --: | --: |
| HLS コンパイル | 40秒 | - |
| 論理合成 | 17分27秒 | 5分8秒 |
]

- HLS 実装の論理合成が長いのは、生成される HDL が複雑なためか
    - 今回の実装では 6,237 行であった

---

## シミュレーション

.flex-center[
| HLS 実装 | HDL 実装 |
| --: | --: |
| 36秒 | 2分45秒 |
]

- HLS は実装をソフトウェアとして実行可能
- HDL は FPGA のエミュレーションが必要

---

.flex[
.column-45[
## HLS のテストベンチ

- Vivado HLS では C++  
で記述可能
- OpenCV も使える!
]
.column-55[
```cpp
cv::Mat dst(HEIGHT, WIDTH, CV_8UC3);

stream_type<PPC> stream1;
fractal(x0, y0, dx, dy, cr, ci,
        stream1);

stream_type<1u> stream2;
split_stream<PPC>(stream1, stream2);
AXIvideo2cvMat(stream2, dst);

// GBR2BGR
auto channels = std::vector<cv::Mat>{};
cv::split(dst, channels);
std::swap(channels[0], channels[1]);
cv::merge(channels, dst);

cv::imwrite(OUTPUT_IMAGE, dst);
```
]
]

---

.flex[
.column-45[
## HDL のテストベンチ

- HDL は HDL で記述
- ファイルの入出力は可能
]
.column-55[
```verilog
logic [7:0] data;

fractal_generator u_0( /* ... */);

always #2500ps clk = ~clk;

initial begin
  #150ns resetn = 1;

  wait (frame_start == 1'b1);

  #100ns $finish();
end
```
]
]

---

.flex[
.column-45[
## HDL のテストベンチ

- HDL は HDL で記述
- ファイルの入出力は可能
]
.column-55[
```verilog
integer file;
initial begin
  file = $fopen("out.pgm", "w");
  $fwrite(file, "P5\n%0d %0d\n%0d\n",
          width, height, 2**8-1);

  while (img_writing == 1) begin
    @(posedge clk)
    #1ns;
    /* ... */
    $fwrite(file, "%c", data);
    /* ... */
  end

  $fclose(file);
  $display("Image written");
end
```
]
]

---

class: no-slide-number

## システム全体のコンパイル時間

.flex-center[
| | 時間 |
| --- | --: |
| 19モジュールの論理合成  | 5分32秒  |
| 論理合成                | 3分29秒  |
| インプリメンテーション  | 15分3秒  |
| Linuxブートイメージ生成 | 14分6秒  |
| 合計                    | 38分10秒 |
]

.footnote.small[\* 最大64並列で実行可能なマシンで測定]

---

## 描画性能

.flex[
.column-35[
| FPGA | [ms] |
| --- | --: |
| HLS 実装 | 165 |
| HDL 実装 | 61.9 |
]
.column-65[
| CPU | [ms] |
| --- | --: |
| APU (4 threads) | 1844.95 |
| Xeon Gold (30 threads) | 91.7955 |
]
]

- CPU 実装はあくまで参考値

---

background-image: url(img/skylake.webp)

---

class: left, section-title-page

.section-title[
.section-number[
6.
]
.section-text[
まとめ
]
]

---

## まとめ

- Ultra96 で動作する Julia 集合の描画システムを実装
    - FPGA に Julia 集合を描画する専用の回路を実装
    - CPU や GPU との協調動作で  
    FPGA だけでは難しい複雑なシステムを実現
- HLS と HDL による FPGA 開発の特徴を紹介

--
- **ハード・ソフトを下から上まで実装すると...楽しい!**

---

exclude: true
class: no-slide-number

## Pyxis 2010

.flex-center[
.column-80.center[
![](img/pyxis2010.jpg)
]
]

.footnote.tiny[Mandelbrot集合描画ハードウエア [Pyxis 2010]: http://www.chiaki.cc/Pyxis2010/index.htm]

---

class: no-slide-number

## Pyxis 2010

.flex-center[
.column-80[
<iframe width="100%" height="512" src="https://www.youtube-nocookie.com/embed/aHp51yPUEaI" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
]
]

.footnote.tiny[Mandelbrot集合描画ハードウエア [Pyxis 2010]: http://www.chiaki.cc/Pyxis2010/index.htm]
