function! Fib(n) abort
  if a:n <= 1
    return a:n
  else
    return a:n * Fib(a:n - 1)
  endif
endfunction

function! FizzBuzz(n) abort
  for l:i in range(1, a:n)
    if l:i % 15 == 0
      echo 'FizzBuzz'
    elseif l:i % 3 == 0
      echo 'Fizz'
    elseif l:i % 5 == 0
      echo 'Buzz'
    else
      echo l:i
    endif
  endfor
endfunction
