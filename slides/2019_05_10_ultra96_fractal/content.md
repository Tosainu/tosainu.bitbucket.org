class: left, middle

# FPGA/CPU混載型SoCを用いた<br />ソフト・ハード協調システムの開発事例

.small[
2019/05/10 - リコンフィギャラブルシステム研究会  
佐藤 健太・佐藤 幸紀 (豊橋技科大)
]

---

- 資料  
.larger[<https://l.myon.info/reconf-1905>]
- 関連ブログ記事 .small[(予定)]  
.larger[<https://blog.myon.info>]

---

## Agenda

1. はじめに
    - FPGA を搭載した SoC の登場
    - Ultra96 を用いた Julia set explorer の実装例
2. Vivado HLS による Julia setアクセラレータの実装
3. Ultra96 向け PetaLinux プロジェクトの作成
4. Julia set アクセラレータの Linux デバイスドライバの実装
5. Julia set 表示ソフトウェアの実装
6. まとめ

---

class: left, section-title-page

.section-title[
.section-number[
1.
]
.section-text[
はじめに
]
]

---

class: no-slide-number

## FPGA を搭載した SoC の登場

- FPGA、プロセッサ、周辺回路を一体化した SoC が登場している
    - Xilinx 社の **Zynq**、Intel 社の **SoC FPGA** など

.flex-center[
.column-40.left[
![](img/zynq.png)
]
.column-30.right[
![](img/agilex.png)
]
]

.footnote.tiny.right[https://www.xilinx.com/products/silicon-devices/soc.html<br />https://www.intel.com/content/www/us/en/products/programmable/soc.html]

---

background-image: url(img/zynq-eg-block.png)
background-position: 50% 80%
background-size: auto 82%
class: no-slide-number

## 例: Zynq UltraScale+ EG

.footnote.tiny.right[https://www.xilinx.com/products/silicon-devices/soc/zynq-ultrascale-mpsoc.html]

---

## FPGA 搭載 SoC の特徴

- ソフト・ハードをそれぞれ専用の回路で動作させることが可能  
➡ **高い性能**と**開発の柔軟性**の両立
- 小型で多機能  
➡ 電力・コスト・スペースの削減

---

background-image: url(img/ultra96.png)
background-position: 90% 85%
background-size: auto 50%
class: no-slide-number

## Ultra96

- Zynq UltraScale+ MPSoC の開発ボードの1つ


- **高性能!** .small[(ZU3EG A484, LPDDR4 2GB, MiniDP, USB 3.0, Wi-Fi & BT, etc.)]
- **小型!** .small[(85mm x 54mm)]
- **安価!** .small[($249)]

.footnote.tiny.right[https://www.96boards.org/product/ultra96/]

---

.flex[
.column-65[
## Julia set explorer 作ってみた

- [github.com/Tosainu/ultra96-fractal](https://github.com/Tosainu/ultra96-fractal)
]
.column-35[
<iframe width="100%" height="620" src="https://www.youtube.com/embed/wADGj1B9DYo" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
]
]

---

## Julia set explorer 特徴

- **Julia set の高速な描画**
    - FPGA 上に専用回路を実装
    - 1920px ✕ 1080px @ **6.05 fps** を達成
- **高機能な表示・操作アプリケーション**
    - Linux デスクトップアプリケーションとして実装
    - ゲームパッドによる移動・拡大・縮小
    - ステータスのオーバーレイ表示

---

class: left, section-title-page

.section-title[
.section-number[
2.
]
.section-text[
Vivado HLS による  
Julia set アクセラレータの実装
]
]

---

background-image: url(img/hls.png)
background-position: 90% 50%
background-size: auto 40%

## Vivado HLS

- Xilinx 社が提供する**高位合成** の開発環境


- C、C++、SystemC を用いた設計が可能
- 豊富なライブラリが提供される
    - `<math.h>` で提供される数学関数
    - OpenCV 互換の関数
    - 任意精度のデータ型

---

background-image: url(img/fractal-ip.png)
background-position: 90% 50%
background-size: auto 40%

## 実装したアクセラレータ

- C++11 で記述
- 計算に固定小数点 (`ap_fixed<32, 4>`) を使用
- 6ステージのパイプライン ✕ 32回路
- 100 MHz で動作
- 1920px ✕ 1080px の画像を 6.05 fps で出力
- 制御用の AXI4-Lite、  
画像出力用の AXI4-Stream ポートを搭載

---

name: opt

## Julia set の色つけ処理

- 描画した Julia set の色つけ処理
\\[
\begin{aligned}
    r(t) &= 9 (1 - t) t^3 \\\\
    g(t) &= 15 (1 - t)^2 t^2 \\\\
    b(t) &= 8.5 (1 - t)^3 t
\end{aligned}
\\]

---

background-image: url(img/color.svg)
background-size: auto 90%
class: no-slide-number

---

template: opt
- 今回の実装では \\(t\\) に渡されうる値が既知 \\((\cfrac{0}{255}, \cfrac{1}{255}, \cfrac{2}{255}, \dots, \cfrac{255}{255}\\))  
➡ **\\(r(t)\\), \\(g(t)\\), \\(b(t)\\) の値を予め計算しておけば...？**

---

## 色つけ処理の最適化 .small[(愚直な方法)]

- \\(i = 0, t = \cfrac{i}{255}\\) として \\(r(t)\\), \\(g(t)\\), \\(b(t)\\) を計算
- 計算結果をソースコードに貼り付ける
- \\(i = 1, 2, \dots, 255\\) の場合も同様に計算・貼り付けを行う

--

count: false

.large[**面倒くさい！**]

---

background-image: url(img/hls-cxx11.png)
background-position: 97.5% 27%
background-size: 42% auto

## 色つけ処理の最適化 .small[(かしこい？方法)]

- Vivado HLS は C++11 が利用可能
    - `CFLAGS` に `-std=c++11` を指定する
--
- C++11 が使えると **`constexpr`** がある

--
- **コンパイル時**に色変換テーブルを構築しよう！

---

## 色つけ処理の最適化結果 .small[(色割り当て処理)]

- 色つけ処理を**コンパイル時に評価可能な関数**として実装
    ```cpp
    // [0,1] の浮動小数点数dを [0,255] の整数に変換する
    constexpr std::uint32_t f2i(double d) { /* ... */ }
    
    constexpr std::uint32_t colorize(double t) {
      return
          // red
          (f2i(9.0 * (1.0 - t) * t * t * t) << 16) |
          // blue
          (f2i(8.5 * (1.0 - t) * (1.0 - t) * (1.0 - t) * t) << 8) |
          // green
          (f2i(15.0 * (1.0 - t) * (1.0 - t) * t * t));
    }
    ```

--
exclude: true
count: false

.overlay-right[
色割り当ての処理が  
**C++ の文法で定義可能**に!
]

---

## 色つけ処理の最適化結果 .small[(色割り当てテーブル構築処理)]

- コンパイル時に色変換テーブルを構築するコードを追加
    ```cpp
    namespace detail { /* constexpr & template magic... */ }
    
    constexpr std::uint32_t index2color(std::size_t i) {
      return colorize(static_cast<double>(i) / 255.0);
    }
    
    // 256 個の配列を構築し各要素を index2color(Index) で初期化する
    constexpr std::array<std::uint32_t, 256> make_color_table() {
      return detail::make_array<256>(index2color);
    }
    
    static constexpr auto color_table = make_color_table();
    ```

--

exclude: true
count: false

.overlay-right[
色変換テーブルを  
**コンパイル時に自動生成可能**に!
]

---

background-image: url(img/hls-result.png)
background-position: 50% -30%
background-size: 75% auto

## 色つけ処理の最適化結果 .small[(FPGA デザイン)]

--

exclude: true
count: false

.overlay-left[
色変換処理を  
**BRAM の Read 1回で実現可能**に!
]

---

## 色つけ処理の最適化結果

- 色つけ処理自体は **C++ の文法で定義可能**に
- 必要な値がコンパイル時に展開され**定数の配列**に
- FPGA の対応する処理は **ROM からの Read 1回で実現可能**に

---

background-image: url(img/bd.png)
background-position: 50% 50%
background-size: 97.5% auto

## ブロックデザイン

---

background-image: url(img/impl-result.png)
background-position: 50% 55%
background-size: 90% auto

## Implementation 結果

---

class: left, section-title-page

.section-title[
.section-number[
3.
]
.section-text[
Ultra96 向け  
PetaLinux プロジェクトの作成
]
]

---

class: no-slide-number

## PetaLinux Tools

- Xilinx の各種プラットフォーム向け Linux イメージ構築ツール
    - ブートローダ、カーネル、デバイスドライバ、ライブラリ、  
    アプリケーションの設定・ビルド
    - ブートイメージの作成
- Zynq UltraScale+ MPSoC, Zynq-7000, MicroBlaze に対応

.footnote.tiny.right[https://www.xilinx.com/products/design-tools/embedded-software/petalinux-sdk.html]


---

## PetaLinux Board Support Packages (BSP)

- <http://zedboard.org/support/design/24166/156>  
![](img/petalinux-bsp.png)
- **v2018.2** 向けなので注意

---

## BSP で PetaLinux プロジェクト作成

- `--source` オプションにダウンロードした BSP ファイルを指定する
    ```
    $ petalinux-create --type project \
                       --source <path-to-bsp> \
                       --name <project-name>
    
    $ cd <project-name>
    $ petalinux-config --get-hw-description=<path-to-hdf-dir>
    ```

---

class: no-slide-number

## PetaLinux プロジェクトの作成

- Zynq UltraScale+ MPSoC 向けプロジェクトを作成する
    ```
    $ petalinux-create --type project \
                       --template zynqMP \
                       --name <project-name>
    
    $ cd <project-name>
    $ petalinux-config --get-hw-description=<path-to-hdf-dir>
    ```

.footnote.tiny.right[https://www.xilinx.com/support/documentation/sw_manuals/xilinx2018_3/ug1144-petalinux-tools-reference-guide.pdf]

---

## Ultra96 向けの設定

- `petalinux-config` で以下の値を設定  
    ```
    DTG Settings  --->
        (zcu100-revc) MACHINE_NAME
    u-boot Configuration  --->
        (xilinx_zynqmp_zcu100_revC_defconfig) u-boot config target
    ```
- Ultra96 向け DeviceTree などが読み込まれるようになる
    - 無線LAN、LED などが正しく認識される
---

class: left, section-title-page

.section-title[
.section-number[
4.
]
.section-text[
Julia set アクセラレータの  
Linux デバイスドライバの実装
]
]

---

## デバイスドライバを実装すると...

- デバイスドライバなしでも制御は可能


--

count: false

- **ユーザ空間での処理が減らせる**
- **Linux の機能との組み合わせが容易になる**

---

## 方針

- 実装した Julia set アクセラレータを  
映像入力デバイスとして扱う Video4Linux (V4L2) ドライバを実装
- V4L2 の API で生成された画像をキャプチャできるようにする

---

## Xilinx Video IP driver

- Xilinx 製 ビデオ関連 IP のドライバ
    - Scaler、MIPI CSI2 Rx Subsystem、Test Pattern Generator など
- [Xilinx/linux-xlnx](https://github.com/Xilinx/linux-xlnx) に含まれている
    ```
    Device Drivers  --->
        <*> Multimedia support  --->
            [*]   V4L platform devices  --->
                <*>   Xilinx Video IP (EXPERIMENTAL)
    ```
- **デバイスドライバに必要な処理**、**DeviceTree の設定項目**などがわかる

---

## PetaLinux にカーネルモジュールを追加する

- カーネルモジュールの追加
    ```
    $ petalinux-create --type modules \
                       --name <module-name> \
                       --enable
    ```
- Boot 時に自動でロードされるようにする
    ```
    $ echo 'KERNEL_MODULE_AUTOLOAD_append = " <module-name>"' \
        >> project-spec/meta-user/conf/petalinuxbsp.conf
    ```

---

class: left, section-title-page

.section-title[
.section-number[
5.
]
.section-text[
Julia set 表示ソフトウェアの実装
]
]

---

background-image: url(img/screenshot.png)
background-position: 325% 55%
background-size: auto 80%

## 実装した表示ソフトウェア

- C++17 で記述
- Wayland client として実装
- ステータスのオーバーレイ表示
- ゲームパッドによる図形の 移動、拡大・縮小

---

## DisplayPort ドライバと X11 の相性が悪い問題

- Ultra96 で X11 な WindowManager (WM) を利用すると
    - 画面がちらつく
    - OpenGL ES 2.0 の性能が出ない
        - `glxgears` で 5 fps 程度
--
- Wayland な WM である **Weston** を利用して解決 (ある程度マシに)

---

## PetaLinux で Wayland & Weston を使う

- PetaLinux には Weston の packagegroup が用意されている
- .small[`project-spec/meta-user/conf/petalinuxbsp.conf`] に以下の行を追加
    ```conf
    DISTRO_FEATURES_append = " wayland"
    DISTRO_FEATURES_remove = " x11"
    IMAGE_INSTALL_append = " packagegroup-petalinux-weston"
    ```
- 出来上がったイメージで Boot させると Weston が起動する

---

## PL で生成した画像を低遅延で表示したい

- ステータスのオーバーレイ表示を実現するため OpenGL を使うことに
- V4L2 でキャプチャした映像を  
OpenGL ES 2.0 で高速に表示するには...？

---

exclude: true
name: display-problems

## PL で生成した画像を低遅延で表示したい

1. **画像フォーマットの問題**
    - アクセラレータが出力する画像のフォーマット: RGB  
    GPU がサポートするテクスチャのフォーマット: RGBA, Mono, YUV
2. **データ転送コストの問題**
    - V4L2 によりキャプチャした画像は **カーネルのメモリ空間** にある
    - **メモリのキャッシュが無効化**されている

---

exclude: true
background-image: url(img/bd.png)
background-position: 110% 200%
background-size: 155% auto


## 1. 画像フォーマットの問題

- AXI4-Stream Subset Converter を用いて  
アクセラレータの出力に alpha channel 相当のデータを挿入
- RGBA/RGBX 画像としてキャプチャするようにして対応

---

exclude: true

## 2. データ転送コストの問題

- **dma-buf** を利用して画像を転送 (共有) することで対応
    - 複数のデバイス間でバッファを共有するための Linux カーネルの機能

.footnote.tiny.right[https://www.kernel.org/doc/html/v4.14/driver-api/dma-buf.html<br />https://elinux.org/images/5/53/Zero-copy_video_streaming.pdf]

---

class: no-slide-number

## GPU へのテクスチャ転送 (OpenGL ES 2.0)

- 画像 `image` を GPU で扱いたいとき

```cpp
GLubyte image[width * height * 4];

GLuint texture;
glGenTextures(num_buffers, &texture);

glBindTexture(GL_TEXTURE_2D, texture);
glTexImage2D(GL_TEXTURE_2D,
    0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
glBindTexture(GL_TEXTURE_2D, 0);
```

.footnote.small[\* 細かな処理は一部省いています]

---

background-image: url(img/nodmabuf.svg)
background-position: 90% 45%
background-size: 40% auto

## `glTexImage2D` に V4L2 のバッファを渡した場合

- ユーザ空間で画像データのコピーが発生
- ソースの V4L2 のバッファは  
キャッシュが無効化されている

⬇

- 1フレームあたり約 **50m ~ 90ms** 程度のオーバーヘッド

---

background-image: url(img/dmabuf.svg)
background-position: 90% 45%
background-size: 40% auto
class: no-slide-number

## dma-buf を利用した場合

- 複数のデバイス間でバッファの共有を可能にする Linux カーネルの機能

⬇

- ユーザ空間の処理は  
**ファイルディスクリプタの転送のみ**
- import 先でメモリ領域が直接参照される **(データのコピーが発生しない)**

.footnote.tiny.right[https://www.kernel.org/doc/html/v4.14/driver-api/dma-buf.html<br />https://elinux.org/images/5/53/Zero-copy_video_streaming.pdf]

---

class: left, section-title-page

.section-title[
.section-number[
6.
]
.section-text[
まとめ
]
]

---

## まとめ

- Ultra96 で動作する Julia set explorer を実装
    - PL に Julia set を描画する専用の回路を実装
    - アクセラレータのデバイスドライバを実装
    - 表示・操作のための Linux デスクトップアプリケーションを実装
    - FPGA 搭載 SoC の高い性能と開発の柔軟性を実証
- 開発に関する情報は意外とある
- 楽しいものを作って流行らせましょう
