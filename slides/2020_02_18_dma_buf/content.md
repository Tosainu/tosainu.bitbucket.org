class: left, middle, no-slide-number

# Julia set explorer を支える技術  
.margin-0[
**dma-buf** によるデバイスドライバ間のバッファ共有
]
.right.small[.circle.inline-img-middle[![cocoa](img/cocoa.svg)] Kenta Sato]

.footnote.smaller[
2020/02/08 - カーネル/VM探検隊@関西 10回目
]

---

class: no-slide-number

## (🌸╹◡╹)ﾉ

.flex[
.column-25[
.circle[![cocoa](img/cocoa.svg)]
]
.column-75[
- Kenta Sato / Tosainu / @myon___
- すき
    - .inline-img-middle[![arch-logo](img/archlinux-logo.svg)]
    - .inline-img-middle[![](img/cpp_logo.svg)] C++17, .inline-img-middle[![](img/rust-logo-blk.svg)] Rust, .inline-img-middle[![](img/haskell.svg)] Haskell
    - ご注文はうさぎですか？
- [myon.info](https://myon.info)
]
]

.footnote.tiny[
https://github.com/isocpp/logos  
https://www.rust-lang.org/policies/media-guide  
https://wiki.haskell.org/Thompson-Wheeler_logo
]

---

## 2010 - 2011 くらいのこと

- **すごい電子工作**している人の Web サイトを探すのが好きだった

---

class: no-slide-number

## Pyxis 2010

.flex-center[
.column-75[
<iframe width="100%" height="515" src="https://www.youtube-nocookie.com/embed/rg9sCIHA6DM" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
]
]

.footnote.tiny[
Mandelbrot集合描画ハードウエア [Pyxis 2010]: http://www.chiaki.cc/Pyxis2010/index.htm
]

---

## あれから約10年

- いろいろあって最近 FPGA を触っています
- せっかくだし FPGA で何かおもしろいもの作りたいな...

---

class: no-slide-number

.flex[
.column-55[
## 作ってみた

- GitHub: [Tosainu/ultra96-fractal](https://github.com/Tosainu/ultra96-fractal)
]
.column-45[
<blockquote class="twitter-tweet" data-dnt="true"><p lang="ja" dir="ltr">一度 HDL 書く開発してみたいなーってことで冬・春に HLS で書いたのを SystemVerilog で書き直してた。<a href="https://t.co/h7HXFqsaHa">https://t.co/h7HXFqsaHa</a> と比較して動作クロック3倍 (100➡300MHz)、描画速度2.67倍 (6➡16fps) に改善できて、色も変えれるようになった。 <a href="https://t.co/GoizfFcpYa">pic.twitter.com/GoizfFcpYa</a></p>&mdash; +。:.ﾟ٩(๑ᐳ◡ᐸ๑)۶:.｡+ﾟ (@myon___) <a href="https://twitter.com/myon___/status/1163835624710795264?ref_src=twsrc%5Etfw">August 20, 2019</a></blockquote>
]
]

---

class: no-slide-number
background-image: url(img/ultra96.png)
background-position: 90% 87.5%
background-size: auto 50%

## Ultra96

- Xilinx の **Zynq UltraScale+ MPSoC** が載ってる開発ボード
- **高性能！** .small[(ZU3EG A484, LPDDR4 2GB, MiniDP, USB 3.0, Wi-Fi & BT, etc.)]
- **小型！** .small[(85mm ✕ 54mm)]
- 比較的**安価！** .small[($249)]
- .inline-img-large-middle[![](img/96Consumer.svg)]

.footnote.tiny[
https://www.96boards.org/product/ultra96/
]

---

class: no-slide-number
background-image: url(img/zynq-eg-block.png)
background-position: 50% 82.5%
background-size: auto 82%

## Zynq UltraScale+ EG

.footnote.tiny[
https://www.xilinx.com/products/silicon-devices/soc/zynq-ultrascale-mpsoc.html
]

--
background-image: url(img/zynq-eg-block-mask-apu.png), url(img/zynq-eg-block.png)

--
background-image: url(img/zynq-eg-block-mask-mem.png), url(img/zynq-eg-block.png)

--
background-image: url(img/zynq-eg-block-mask-gpu.png), url(img/zynq-eg-block.png)

--
background-image: url(img/zynq-eg-block-mask-pl.png), url(img/zynq-eg-block.png)

---

class: no-slide-number
background-image: url(img/zu_block.svg)
background-position: 50% 0%

.footnote.tiny[
https://www.xilinx.com/support/documentation/user_guides/ug1085-zynq-ultrascale-trm.pdf
]

--
background-position: 50% 25%
count: false

--
background-position: 50% 50%
count: false

--
background-position: 50% 75%
count: false

--
background-position: 50% 100%
count: false

---

name: jse-arch
background-image: url(img/block.svg)
background-position: 50% 75%
background-size: 95% auto

## FPGA ✕ CPU ✕ GPU = 💪

---

template: jse-arch
background-image: url(img/block_mask_fpga.svg), url(img/block.svg)

- **FPGA**: Julia 集合描画のアクセラレータ

---

template: jse-arch
background-image: url(img/block_mask_apu.svg), url(img/block.svg)

- **APU**: Linux カーネル、デバイスドライバ、制御アプリケーション

---

template: jse-arch
background-image: url(img/block_mask_gpu.svg), url(img/block.svg)

- **GPU**: 画像の合成とディスプレイへの出力

---

template: jse-arch
background-image: none

- **Julia 集合の高速描画**
    - 1920px ✕ 1080px @ **16.2 fps** を達成
- **ゲームパッドによるインタラクティブな操作**
- **描画パラメータのステータス表示**

---

## やったこと

1. Julia 集合アクセラレータを FPGA に実装
2. 1. と APU、その他周辺回路との接続を定義
3. 1. のデバイスドライバを実装
4. 制御・表示のためのアプリケーションを実装
5. 1 ~ 4 をまとめた Linux のブートイメージを作成

---

## 今日はなすこと

1. Julia 集合アクセラレータを FPGA に実装
2. 1. と APU、その他周辺回路との接続を定義
3. 1. のデバイスドライバを実装
4. **制御・表示のためのアプリケーションを実装** .small[(の一部)]
5. 1 ~ 4 をまとめた Linux のブートイメージを作成

---

## FPGA が描いた画像を表示するには？

- **FPGA が描いた画像は Video4Linux2 の API で取れる**
    - デバイスドライバの実装により `/dev/video0` が生えた
- **GPU (Mali-400 MP2) は OpenGL ES 2.0 が使える**
    - libdrm, Mesa GBM で OpenGL/EGL の context を直接初期化
    - X11 や Wayland を通すと性能が出なかったので...

---

class: no-slide-number

## Naive な実装 .smaller[(1/4)]

```c
int fd = open("/dev/video0", O_RDWR);

// V4L2 で使うバッファの種類 (MMAP) と数 (MAX_BUFFERS) などを設定
struct v4l2_requestbuffers req;
req.count = MAX_BUFFERS;
req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
req.memory = V4L2_MEMORY_MMAP;
ioctl(fd, VIDIOC_REQBUFS, &req);

unsigned int num_buffers = req.count;
```

.footnote.tiny[
https://www.kernel.org/doc/html/v5.5/media/uapi/v4l/mmap.html#mmap
]

---

class: no-slide-number

## Naive な実装 .smaller[(2/4)]

```c
void** buffers = malloc(sizeof(void*) * num_buffers);

for (unsigned int i = 0; i < num_buffers; ++i) {
  struct v4l2_buffer buf;
  buf.index = i;
  buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  buf.memory = V4L2_MEMORY_MMAP;
  ioctl(fd, VIDIOC_QUERYBUF, &buf);

  buffers[i] = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED,
                    fd, buf.m.offset);

  ioctl(fd, VIDIOC_QBUF, &buf); // キューへ追加する
}
```

.footnote.tiny[
https://www.kernel.org/doc/html/v5.5/media/uapi/v4l/mmap.html#mmap
]

---

## Naive な実装 .smaller[(3/4)]

```c
// OpenGL のテクスチャを準備
GLuint texture;
glGenTextures(1, &texture);

// V4L2 によるキャプチャを開始
enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
ioctl(fd, VIDIOC_STREAMON, &type);
```

---

class: no-slide-number

## Naive な実装 .smaller[(4/4)]

```c
struct v4l2_buffer buf;
buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
buf.memory = V4L2_MEMORY_MMAP;
ioctl(fd, VIDIOC_DQBUF, &buf); // キューからバッファを取り出して

// バッファ i の画像を GPU に転送 & 表示
glBindTexture(GL_TEXTURE_2D, texture);
glTexSubImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
                0, GL_RGBA, GL_UNSIGNED_BYTE, buffers[buf.index]);
// ...

ioctl(fd, VIDIOC_QBUF, &buf);  // バッファをキューへ戻す
```

.footnote.smaller[
\* `VIDIOC_DQBUF` はブロックするので、必要に応じて `select` や `epoll_wait` を使うとよい
]

---

## 遅い！！！ ヾ(｡>﹏<｡)ﾉﾞ

- `glTexSubImage2D` に **50 ~ 90ms** 程度かかってしまう
    - 1920px ✕ 1080px ✕ 4ch (RGBA) ≅ 7.9 MB
- 画像の変換処理が入るとさらに遅くなる
    - 例: RGB ➡️ RGBA だと **170ms** くらい

---

background-image: url(img/nodmabuf.svg)
background-position: 50% 75%
background-size: 85% auto

## `glTexSubImage2D` を使うケース

- FPGA が出力した画像を GPU に伝える過程で**コピーが発生**
    - しかもキャッシュが無効化されている？

---

class: no-slide-number

## dma-buf

- 複数のデバイスドライバ間でバッファを共有する仕組み .small[(Linux 3.3~)]
- hybrid graphics で使う PRIME のために追加されたっぽい...？
- 共有元からバッファ情報 (`struct dma_buf`) への  
ファイルディスクリプタを **export**、共有先で **import** する

.footnote.tiny[
https://www.kernel.org/doc/html/v5.5/driver-api/dma-buf.html
]

---

background-image: url(img/dmabuf.svg)
background-position: 50% 75%
background-size: 85% auto

## dma-buf を使ったケース

- ユーザ空間でのやり取りは**ファイルディスクリプタのみ**
- GPU のドライバは**共有元のバッファを読める**

---

class: no-slide-number

## `ioctl VIDIOC_EXPBUF`

```c
int* buffer_fds = malloc(sizeof(int) * num_buffers);

for (unsigned int i = 0; i < num_buffers; ++i) {
  // バッファ i の fd を export
  struct v4l2_exportbuffer exbuf;
  exbuf.index = i;
  exbuf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
  ioctl(fd, VIDIOC_EXPBUF, &exbuf);

  buffer_fds[i] = exbuf.fd;
}
```

.footnote.tiny[
https://www.kernel.org/doc/html/v5.5/media/uapi/v4l/vidioc-expbuf.html
]


---

class: no-slide-number

## `EGL_EXT_image_dma_buf_import` .smaller[(1/4)]

```c
// 必要な関数を呼べるようにする
PFNEGLCREATEIMAGEKHRPROC eglCreateImageKHR
    = (PFNEGLCREATEIMAGEKHRPROC)
        eglGetProcAddress("eglCreateImageKHR");

PFNGLEGLIMAGETARGETTEXTURE2DOESPROC glEGLImageTargetTexture2DOES
    = (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC)
        eglGetProcAddress("glEGLImageTargetTexture2DOES");

// OpenGL のテクスチャを準備
GLuint textures[MAX_BUFFERS];
glGenTextures(num_buffers, textures);
```

.footnote.tiny[
https://www.khronos.org/registry/EGL/extensions/EXT/EGL_EXT_image_dma_buf_import.txt  
https://www.khronos.org/registry/EGL/extensions/KHR/EGL_KHR_image_base.txt  
https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external.txt
]

---

## `EGL_EXT_image_dma_buf_import` .smaller[(2/4)]

```c
for (unsigned int i = 0; i < num_buffers; ++i) {
  EGLint attrs[] = {
    EGL_IMAGE_PRESERVED_KHR,       EGL_TRUE,
    EGL_WIDTH,                     width,               // サイズ
    EGL_HEIGHT,                    height,
    EGL_LINUX_DRM_FOURCC_EXT,      DRM_FORMAT_ABGR8888, // フォーマット
    EGL_DMA_BUF_PLANE0_FD_EXT,     buffer_fds[i],       // dma-buf の fd
    EGL_DMA_BUF_PLANE0_OFFSET_EXT, 0,
    EGL_DMA_BUF_PLANE0_PITCH_EXT,  width * 4,
    EGL_NONE
  };

  // ...
```

---

## `EGL_EXT_image_dma_buf_import` .smaller[(3/4)]

```c
for (unsigned int i = 0; i < num_buffers; ++i) {
  EGLint attrs[] = { /* ... */ };

  // バッファ i の dma-buf fd から EGLImage を作って
  EGLImageKHR image = eglCreateImageKHR(egl_dpy, EGL_NO_CONTEXT,
                                        EGL_LINUX_DMA_BUF_EXT, NULL, attrs);

  // テクスチャ i に割り当てる
  glBindTexture(GL_TEXTURE_EXTERNAL_OES, textures[i]);
  glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, image);
  glBindTexture(GL_TEXTURE_EXTERNAL_OES, 0);
}
```

---

## `EGL_EXT_image_dma_buf_import` .smaller[(4/4)]

```c
struct v4l2_buffer buf;
buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
buf.memory = V4L2_MEMORY_MMAP;
ioctl(fd, VIDIOC_DQBUF, &buf); // キューからバッファを取り出して

// バッファ i に対応するテクスチャ i を表示
glBindTexture(GL_TEXTURE_EXTERNAL_OES, textures[buf.index]);
// ...

ioctl(fd, VIDIOC_QBUF, &buf);  // バッファをキューへ戻す
```

---

## まとめ・これから

- **dma-buf**: デバイスドライバ間でバッファ共有をする仕組み
- V4L2, OpenGL, EGL における実装例を紹介
    - Julia set explorer では性能改善につながった
- CPU ➡️ FPGA なケースでの dma-buf を検証したい
    - USB 接続の Web カメラの映像を FPGA で処理するなど
- デバドラ側の実装、映像データ以外のやり取りも試してみたい

---

## リンク

.small[
- Ultra96 で Julia set をぐりぐり動かせるやつを作った | Tosainu Lab  
<https://blog.myon.info/entry/2019/05/15/ultra96-julia-set-explorer/>
- Ultra96 で Julia set をぐりぐり動かせるやつをもう少し強くした | Tosainu Lab  
<https://blog.myon.info/entry/2019/08/29/ultra96-julia-set-explorer-2/>
- Zero-Copy Video Streaming on Embedded Systems the Easy Way  
<https://elinux.org/images/5/53/Zero-copy_video_streaming.pdf>
]
