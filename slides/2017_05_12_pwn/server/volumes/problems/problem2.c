#include <stdio.h>
#include <stdlib.h>

__attribute__((constructor)) static void init() {
  setvbuf(stdin, 0, 2, 0);
  setvbuf(stdout, 0, 2, 0);
}

static void dummy() {
  system("echo \"HaHaHa :)\"");
}

char favorite_soft[256];

int main() {
  char name[16];
  printf("What's your name?\n");
  scanf("%s", name);
  printf("What's your favorite software?\n");
  scanf("%255s", favorite_soft);
  printf("Thank you, %s!\n", name);
}
