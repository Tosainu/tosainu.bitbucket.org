#!/usr/bin/gnuplot

set terminal svg size 960, 600
set output 'color.svg'

r(x)=9*(1-x)*x**3
g(x)=15*((1-x)**2)*(x**2)
b(x)=8.5*((1-x)**3)*x
set palette model RGB functions r(gray),g(gray),b(gray)
test palette
