layout: true
class: left, middle
---

# .item-large.vim-logo[![vim-logo](/slides/2018_10_19_vim_nakayoku/assets/vimlogo.svg)] ともっとなかよくなる

.small[
2018/10/19 - TUTLT Vol.1x  
Kenta Sato
]

---

## About .circle.item.item-middle[![cocoa](/slides/2018_10_19_vim_nakayoku/assets/cocoa.svg)] ?

---

layout: false

## About me ?

.flex[
.column-25.center[
.circle.img-80[![cocoa](/slides/2018_10_19_vim_nakayoku/assets/cocoa.svg)]
]
.column-75[
- Kenta Sato / Tosainu
- B4 3系
- Programming (Rust, Haskell, C++17)
- CTF (pwn)
- [.item.item-middle[![arch-logo](/slides/2018_10_19_vim_nakayoku/assets/archlinux-logo-dark-scalable.svg)]](https://www.archlinux.org/)
- [.item.item-middle[![gochiusa-logo](/slides/2018_10_19_vim_nakayoku/assets/gochiusa.png)]](http://www.gochiusa.com/)
]
]

.footnote.small[➡ [myon.info](https://myon.info)]

---

## 突然ですが

Q1: テキストエディタ何使ってますか？

.footnote.small[戦争は始めないでください...]

---

## 突然ですが

Q2: そのテキストエディタにバグを見つけたら...?

---
layout: true
class: center, middle
---

background-image: url(/slides/2018_10_19_vim_nakayoku/assets/patch1515-1.png)

---

count: false

background-image: url(/slides/2018_10_19_vim_nakayoku/assets/patch1515-2.png)

---

layout: false

## Vim patch 8.0.1515

Vim の patch author になりました🌟

---

## はなすこと

1. Vim をデバッグする
2. Vim にパッチを送る
3. Vim のソースコードを読む

---
layout: true
class: left, middle
---

## 1. Vim をデバッグする

---

layout: false

## Vim をデバッグビルドする

- [`src/Makefile` の590行目付近](https://github.com/vim/vim/blob/04c86d27fed5757ae40246d7bb3fdcd0c1959468/src/Makefile#L594)を編集

    ```makefile
    # COMPILER FLAGS - change as you please. Either before running {{{1
    # configure or afterwards. For examples see below.
    # When using -g with some older versions of Linux you might get a
    # statically linked executable.
    # When not defined, configure will try to use -O2 -g for gcc and -O for cc.
    CFLAGS = -g
    #CFLAGS = -O
    ```

---

## Vim をデバッグビルドする

- ビルド

    ```
    $ ./configure --prefix=/usr
    $ make
    ```
- とりあえずビルドした Vim を起動してみる

    ```
    $ ./src/vim
    ```

---

## Vim をデバッグビルドする


- runtime ファイルのディレクトリ問題
    - インストールせずに実行すると runtime が読めなくてエラー
    - `--prefix=` にシステムと同じものを設定する
    - `$VIMRUNTIME` を指定する

---

## Vim をデバッグの前に

- `vimrc` やプラグインは無効化 (最小限に) する
    - Vim 本体開発にプラグイン起因のバグを報告すると  
    ブチ切れられる
- `--clean` オプションが便利
    - `:h --clean`

---

## Vim をデバッグする

- ptrace できるようにする

    ```
    $ sudo sysctl -w kernel.yama.ptrace_scope=0
    ```
- デバッグ対象 (Vim) を起動

    ```
    $ ./src/vim --clean
    ```
- gdb で attach

    ```
    $ gdb -p $(pgrep vim)
    ```

---
layout: true
class: left, middle
---

## 2. Vim にパッチを送る

---

layout: false

## github.com/vim/vim

- Vim のリポジトリが Google Code から GitHub に移行した
- Issue や Pull request も受け付けるように
  - 開発モデル (?) は今までどおり
  - Bram 氏が patch をコミットしていく

---

## Vim の開発コミュニティ

- [groups.google.com/forum/#!forum/vim_dev](https://groups.google.com/forum/#!forum/vim_dev)
    - 一応使われている
- [github.com/vim/vim](https://github.com/vim/vim)
    - 実質メイン
- [github.com/vim-jp/issues/issues](https://github.com/vim-jp/issues/issues)
    - 日本の Vim コミュニティ

---
layout: true
class: left, middle
---

## 3. Vim のソースコードを読む

---

layout: false

## Vim のソースコードを読む

- Vim のソースコードをいきなり読むのはたぶんキツイ
    - そこそこのボリュームがある
    - 各種プラットフォーム対応のためのコード
    - 巨大な `switch`

--

✧\*。ヾ(｡>﹏<｡)ﾉﾞ。\*✧

---

background-image: url(/slides/2018_10_19_vim_nakayoku/assets/brammool.png)

---

background-image: url(/slides/2018_10_19_vim_nakayoku/assets/timeline.png)

---

## Vim のソースコードに慣れる

- Vim の patch を追いかけよう
    - Vim の普段の開発ペースはそこまで早くない (数patch/day)
    - コミットメッセージから、  
    変更されたファイルの役割がなんとなくわかる
- [Bram 氏](https://github.com/brammool)を follow する
- [github.com/vim/vim/commits/master.atom](https://github.com/vim/vim/commits/master.atom) を Watch

---
layout: true
class: left, middle
---

## まとめ

---

layout: false

## まとめ

- Vim の中身を知ってもっとなかよくなろう
- Vim 本体の開発に関するドキュメントは Makefile にもある
- バグ報告・パッチ送付は Vim の GitHub リポジトリから
- Vim の patch を読んでソースコードに慣れよう
